<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengajuansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('mahasiswa_id')->constrained('mahasiswas')->onDelete('cascade');
            $table->string('status', 50);
            $table->text('note')->nullable();
            $table->string('tempat_lahir', 100);
            $table->date('tanggal_lahir');
            $table->string('kebangsaan', 100);
            $table->text('alamat_asal');
            $table->string('kota_asal', 100);
            $table->string('provinsi_asal');
            $table->string('negara_asal');
            $table->string('kode_pos_asal', 50);
            $table->text('alamat_tinggal');
            $table->string('kota_tinggal');
            $table->string('provinsi_tinggal');
            $table->string('kode_pos_tinggal', 50);
            $table->string('jenjang', 20);
            $table->string('angkatan', 20);
            $table->string('prodi', 100);
            $table->string('mulai_ijin_belajar');
            $table->string('lama_ijin_belajar');
            $table->string('no_paspor', 50);
            $table->date('tgl_pengajuan_paspor');
            $table->date('tgl_berakhir');
            $table->string('jenis_pengadaan', 50);
            $table->string('penyedia_beasiswa');
            $table->string('jabatan_penjamin', 50);
            $table->string('surat_jaminan_keuangan');
            $table->string('surat_pernyataan');
            $table->string('surat_kesehatan');
            $table->string('letter_of_accept');
            $table->string('ijasah_terakhir');
            $table->string('surat_ijin_belajar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuans');
    }
}
