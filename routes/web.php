<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\PengajuanController;
use App\Http\Middleware\SuperAdmin;
use App\Http\Middleware\User;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthController::class, 'login'])->name('login');
Route::post('/postlogin', [AuthController::class, 'postLogin']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/register', [AuthController::class, 'postRegister']);
Route::get('/logout', [AuthController::class, 'logout']);
Route::get('/forgot-password', [AuthController::class, 'forgotPassword']);
Route::post('/postForgotPassword', [AuthController::class, 'postForgotPassword']);
Route::get('/reset-password/{token}', [AuthController::class, 'resetPassword']);
Route::post('/postResetPassword', [AuthController::class, 'postResetPassword']);

Route::middleware([SuperAdmin::class])->group(function(){
    Route::get('/user/export', [UserController::class, 'export'])->name('user.export');
    Route::resource('/user', UserController::class);
    Route::resource('/admin', AdminController::class);
    Route::get('/submission/export', [PengajuanController::class, 'export'])->name('submission.export');
    Route::get('/submission/create', [PengajuanController::class, 'create'])->name('submission.create');
    Route::get('/submission', [PengajuanController::class, 'index'])->name('submission.index');
    Route::delete('/submission/{id}', [PengajuanController::class, 'destroy'])->name('submission.destroy');
});

Route::middleware([User::class])->group(function(){
    Route::resource('/home', DashboardController::class);
    Route::post('/submission', [PengajuanController::class, 'store'])->name('submission.store');
});

Route::get('/submission/create', [PengajuanController::class, 'create'])->name('submission.create');

Route::get('/submission/{id}', [PengajuanController::class, 'show'])->name('submission.show');
Route::get('/submission/{id}/edit', [PengajuanController::class, 'edit'])->name('submission.edit');
Route::put('/submission/{id}', [PengajuanController::class, 'update'])->name('submission.update');
Route::get('/information', [PengajuanController::class, 'informasi'])->name('information.show');
