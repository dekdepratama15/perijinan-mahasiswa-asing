<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengajuan extends Model
{
    use HasFactory;
    protected $fillable = [
            'mahasiswa_id',
            'status',
            'note',
            'tempat_lahir',
            'tanggal_lahir',
            'kebangsaan',
            'alamat_asal',
            'kota_asal',
            'provinsi_asal',
            'negara_asal',
            'kode_pos_asal',
            'alamat_tinggal',
            'kota_tinggal',
            'provinsi_tinggal',
            'kode_pos_tinggal',
            'jenjang',
            'angkatan',
            'prodi',
            'mulai_ijin_belajar',
            'lama_ijin_belajar',
            'no_paspor',
            'tgl_pengajuan_paspor',
            'tgl_berakhir',
            'jenis_pengadaan',
            'penyedia_beasiswa',
            'jabatan_penjamin',
            'surat_jaminan_keuangan',
            'surat_pernyataan',
            'surat_kesehatan',
            'letter_of_accept',
            'ijasah_terakhir',
            'surat_ijin_belajar',
            'foto'
    ];

    public function mahasiswa() {
        return $this->belongsTo(Mahasiswa::class, 'mahasiswa_id');
    }


    public function pengajuan_logs() {
        return $this->hasMany(PengajuanLog::class);
    }
}
