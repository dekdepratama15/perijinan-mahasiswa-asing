<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'nama_lengkap',
        'no_telp',
        'jenis_kelamin',
        'agama',
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
}
