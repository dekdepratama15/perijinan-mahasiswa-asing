<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PengajuanLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'pengajuan_id',
        'status_lama',
        'status_baru',
        'tanggal',
        'user_change',
        'user_change_role',
        'note',
];
}
