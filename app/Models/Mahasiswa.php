<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'nama_lengkap',
        'no_telp',
        'jenis_kelamin',
        'agama',
        'nama_ayah',
        'nama_ibu',
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function pengajuan() {
        return $this->hasOne(Pengajuan::class);
    }
}
