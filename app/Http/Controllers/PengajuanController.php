<?php

namespace App\Http\Controllers;

use App\Models\Pengajuan;
use App\Models\Admin;
use App\Models\User;
use App\Models\Mahasiswa;
use Illuminate\Http\Request;
use Alert;
use App\Exports\ExportSubmission;
use Maatwebsite\Excel\Facades\Excel;

class PengajuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengajuans = Pengajuan::with('mahasiswa')->get();
        return view("pengajuan.index", compact('pengajuans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::where('id',auth()->user()->id)->with('mahasiswaDetail')->first();
        $pengajuan = Pengajuan::where('mahasiswa_id', $user->mahasiswaDetail->id)->with('mahasiswa.user')->first();
        // dd($pengajuan->count());
        $page = 'create';
        if ($pengajuan != null && $pengajuan->count() > 0){
            $page = 'show';
        }
        return view("pengajuan.insert", compact('user', 'page', 'pengajuan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            "agama" => "required",
            "tempat_lahir" => "required",
            "tanggal_lahir" => "required",
            "kebangsaan" => "required",
            "alamat_asal" => "required",
            "kota_asal" => "required",
            "provinsi_asal" => "required",
            "negara_asal" => "required",
            "kode_pos_asal" => "required",
            "alamat_tinggal" => "required",
            "kota_tinggal" => "required",
            "provinsi_tinggal" => "required",
            "kode_pos_tinggal" => "required",
            "jenjang" => "required",
            "angkatan" => "required",
            "prodi" => "required",
            "mulai_ijin_belajar" => "required",
            "lama_ijin_belajar" => "required",
            "no_paspor" => "required",
            "tgl_pengajuan_paspor" => "required",
            "tgl_berakhir" => "required",
            "jenis_pengadaan" => "required",
            "penyedia_beasiswa" => "required",
            "jabatan_penjamin" => "required",
            "surat_jaminan_keuangan" => "required",
            "surat_pernyataan" => "required",
            "surat_kesehatan" => "required",
            "letter_of_accept" => "required",
            "ijasah_terakhir" => "required",
            "foto" => "required",
        ];

        $msg = [
            'agama.required' => 'Religion is required',
            'tempat_lahir.required' => 'Place of Birth is required',
            'tanggal_lahir.required' => 'Date of Birth is required',
            'kebangsaan.required' => 'Nationality is required',
            'alamat_asal.required' => 'Address is required',
            'kota_asal.required' => 'City is required',
            'provinsi_asal.required' => 'Province is required',
            'negara_asal.required' => 'Country is required',
            'kode_pos_asal.required' => 'Postal Code is required',
            'alamat_tinggal.required' => 'Address is required',
            'kota_tinggal.required' => 'City is required',
            'provinsi_tinggal.required' => 'Province is required',
            'kode_pos_tinggal.required' => 'Postal Code is required',
            'jenjang.required' => 'Education Level is required',
            'angkatan.required' => 'Level of Study is required',
            'prodi.required' => 'Study Program is required',
            'mulai_ijin_belajar.required' => 'Start Learning is required',
            'lama_ijin_belajar.required' => 'Long Study Permit is required',
            'no_paspor.required' => 'Passport Number is required',
            'tgl_pengajuan_paspor.required' => 'Date of Issue is required',
            'tgl_berakhir.required' => 'Date of Expired is required',
            'jenis_pengadaan.required' => 'Type of Funding is required',
            'penyedia_beasiswa.required' => 'Scholarship Provider is required',
            'jabatan_penjamin.required' => 'Guarantor Position is required',
            'surat_jaminan_keuangan.required' => 'Financial Guarantee Letter is required',
            'surat_pernyataan.required' => 'Statement Letter is required',
            'surat_kesehatan.required' => 'Health Document is required',
            'letter_of_accept.required' => 'Letter of Acceptance is required',
            'ijasah_terakhir.required' => 'Latest Study Certificate is required',
            'foto.required' => 'Photo is required',
        ];
        

        $request->validate($rules, $msg);
        $user = User::where('id',auth()->user()->id)->with('mahasiswaDetail')->first();
        $mahasiswa = Mahasiswa::where('user_id', auth()->user()->id)->first();

        if ($request->file('surat_jaminan_keuangan')){
            $extension  = $request->file('surat_jaminan_keuangan')->getClientOriginalExtension();
            $filename = $user->nim .'-financial-guarantee-letter-'.rand(0, 999). now()->timestamp . '.' . $extension;
            $path = $request->file('surat_jaminan_keuangan')->storeAs('public/files/pengajuan/surat_jaminan_keuangan', $filename);
            $path = str_replace('public', '/storage', $path);
            $surat_jaminan_keuangan = $path;
        }
        if ($request->file('surat_pernyataan')){
            $extension  = $request->file('surat_pernyataan')->getClientOriginalExtension();
            $filename = $user->nim .'-statement-letter-'.rand(0, 999). now()->timestamp . '.' . $extension;
            $path = $request->file('surat_pernyataan')->storeAs('public/files/pengajuan/surat_pernyataan', $filename);
            $path = str_replace('public', '/storage', $path);
            $surat_pernyataan = $path;
        }
        if ($request->file('surat_kesehatan')){
            $extension  = $request->file('surat_kesehatan')->getClientOriginalExtension();
            $filename = $user->nim .'-health-document-'.rand(0, 999). now()->timestamp . '.' . $extension;
            $path = $request->file('surat_kesehatan')->storeAs('public/files/pengajuan/surat_kesehatan', $filename);
            $path = str_replace('public', '/storage', $path);
            $surat_kesehatan = $path;
        }
        if ($request->file('letter_of_accept')){
            $extension  = $request->file('letter_of_accept')->getClientOriginalExtension();
            $filename = $user->nim .'-letter-of-accept-'.rand(0, 999). now()->timestamp . '.' . $extension;
            $path = $request->file('letter_of_accept')->storeAs('public/files/pengajuan/letter_of_accept', $filename);
            $path = str_replace('public', '/storage', $path);
            $letter_of_accept = $path;
        }
        if ($request->file('ijasah_terakhir')){
            $extension  = $request->file('ijasah_terakhir')->getClientOriginalExtension();
            $filename = $user->nim .'-latest-study-certificate-'.rand(0, 999). now()->timestamp . '.' . $extension;
            $path = $request->file('ijasah_terakhir')->storeAs('public/files/pengajuan/ijasah_terakhir', $filename);
            $path = str_replace('public', '/storage', $path);
            $ijasah_terakhir = $path;
        }

        if ($request->file('foto')){
            $extension  = $request->file('foto')->getClientOriginalExtension();
            $filename = $user->nim .'-photo-'.rand(0, 999). now()->timestamp . '.' . $extension;
            $path = $request->file('foto')->storeAs('public/files/pengajuan/foto', $filename);
            $path = str_replace('public', '/storage', $path);
            $foto = $path;
        }

        $mahasiswa->agama = $request->agama;
        $mahasiswa->save();

        $data = [
            'mahasiswa_id' => $user->mahasiswaDetail->id,
            'status' => $request->status,
            'note' => $request->note,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'kebangsaan' => $request->kebangsaan,
            'alamat_asal' => $request->alamat_asal,
            'kota_asal' => $request->kota_asal,
            'provinsi_asal' => $request->provinsi_asal,
            'negara_asal' => $request->negara_asal,
            'kode_pos_asal' => $request->kode_pos_asal,
            'alamat_tinggal' => $request->alamat_tinggal,
            'kota_tinggal' => $request->kota_tinggal,
            'provinsi_tinggal' => $request->provinsi_tinggal,
            'kode_pos_tinggal' => $request->kode_pos_tinggal,
            'jenjang' => $request->jenjang,
            'angkatan' => $request->angkatan,
            'prodi' => $request->prodi,
            'mulai_ijin_belajar' => $request->mulai_ijin_belajar,
            'lama_ijin_belajar' => $request->lama_ijin_belajar,
            'no_paspor' => $request->no_paspor,
            'tgl_pengajuan_paspor' => $request->tgl_pengajuan_paspor,
            'tgl_berakhir' => $request->tgl_berakhir,
            'jenis_pengadaan' => $request->jenis_pengadaan,
            'penyedia_beasiswa' => $request->penyedia_beasiswa,
            'jabatan_penjamin' => $request->jabatan_penjamin,
            'surat_jaminan_keuangan' => $surat_jaminan_keuangan,
            'surat_pernyataan' => $surat_pernyataan,
            'surat_kesehatan' => $surat_kesehatan,
            'letter_of_accept' => $letter_of_accept,
            'ijasah_terakhir' => $ijasah_terakhir,
            'foto' => $foto,
            'status' => 'checking',
        ];
        
        $pengajuan = Pengajuan::create($data);
        $this->createPengajuanLog([
            'pengajuan_id' => $pengajuan->id,
            'status_lama' => 'new',
            'status_baru' => 'new',
            'tanggal' => date('Y-m-d H:i:s'),
            'user_change' => $user->mahasiswaDetail->nama_lengkap,
            'user_change_role' => $user->role,
        ]);

        
        Alert::success('Saved', 'Submission Data Saved Succesfuly!');
        return redirect('submission/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pengajuan  $pengajuan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pengajuan = Pengajuan::where('id', $id)->with('mahasiswa.user')->first();
        $page = 'show';
        return view("pengajuan.insert", compact('pengajuan', 'page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pengajuan  $pengajuan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pengajuan = Pengajuan::where('id', $id)->with('mahasiswa.user')->first();
        $page = 'edit';
        return view("pengajuan.insert", compact('pengajuan', 'page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pengajuan  $pengajuan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            "agama" => "required",
            "tempat_lahir" => "required",
            "tanggal_lahir" => "required",
            "kebangsaan" => "required",
            "alamat_asal" => "required",
            "kota_asal" => "required",
            "provinsi_asal" => "required",
            "negara_asal" => "required",
            "kode_pos_asal" => "required",
            "alamat_tinggal" => "required",
            "kota_tinggal" => "required",
            "provinsi_tinggal" => "required",
            "kode_pos_tinggal" => "required",
            "jenjang" => "required",
            "angkatan" => "required",
            "prodi" => "required",
            "mulai_ijin_belajar" => "required",
            "lama_ijin_belajar" => "required",
            "no_paspor" => "required",
            "tgl_pengajuan_paspor" => "required",
            "tgl_berakhir" => "required",
            "jenis_pengadaan" => "required",
            "penyedia_beasiswa" => "required",
            "jabatan_penjamin" => "required",
            "status" => "required",
        ];

        $msg = [
            'agama.required' => 'Religion is required',
            'tanggal_lahir.required' => 'Date of Birth is required',
            'kebangsaan.required' => 'Nationality is required',
            'alamat_asal.required' => 'Address is required',
            'kota_asal.required' => 'City is required',
            'provinsi_asal.required' => 'Province is required',
            'negara_asal.required' => 'Country is required',
            'kode_pos_asal.required' => 'Postal Code is required',
            'alamat_tinggal.required' => 'Address is required',
            'kota_tinggal.required' => 'City is required',
            'provinsi_tinggal.required' => 'Province is required',
            'kode_pos_tinggal.required' => 'Postal Code is required',
            'jenjang.required' => 'Education Level is required',
            'angkatan.required' => 'Level of Study is required',
            'prodi.required' => 'Study Program is required',
            'mulai_ijin_belajar.required' => 'Start Learning is required',
            'lama_ijin_belajar.required' => 'Long Study Permit is required',
            'no_paspor.required' => 'Passport Number is required',
            'tgl_pengajuan_paspor.required' => 'Date of Issue is required',
            'tgl_berakhir.required' => 'Date of Expired is required',
            'jenis_pengadaan.required' => 'Type of Funding is required',
            'penyedia_beasiswa.required' => 'Scholarship Provider is required',
            'jabatan_penjamin.required' => 'Guarantor Position is required',
            'status.required' => 'Status is required',
        ];

        
        if($request->input('status') == 'complete'){
            $rules['surat_ijin_belajar'] = 'required';
            $msg['surat_ijin_belajar.required'] = 'Study Permit Letter is required';
        }
        

        $request->validate($rules, $msg); 

        $cek = Pengajuan::find($id);
        $mahasiswa = Mahasiswa::find($cek->mahasiswa_id);
        $user = User::find($mahasiswa->user_id);
        $mahasiswa->agama = $request->agama;
        $mahasiswa->save();

        $data = [
            'status' => $request->status,
            'note' => $request->note,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'kebangsaan' => $request->kebangsaan,
            'alamat_asal' => $request->alamat_asal,
            'kota_asal' => $request->kota_asal,
            'provinsi_asal' => $request->provinsi_asal,
            'negara_asal' => $request->negara_asal,
            'kode_pos_asal' => $request->kode_pos_asal,
            'alamat_tinggal' => $request->alamat_tinggal,
            'kota_tinggal' => $request->kota_tinggal,
            'provinsi_tinggal' => $request->provinsi_tinggal,
            'kode_pos_tinggal' => $request->kode_pos_tinggal,
            'jenjang' => $request->jenjang,
            'angkatan' => $request->angkatan,
            'prodi' => $request->prodi,
            'mulai_ijin_belajar' => $request->mulai_ijin_belajar,
            'lama_ijin_belajar' => $request->lama_ijin_belajar,
            'no_paspor' => $request->no_paspor,
            'tgl_pengajuan_paspor' => $request->tgl_pengajuan_paspor,
            'tgl_berakhir' => $request->tgl_berakhir,
            'jenis_pengadaan' => $request->jenis_pengadaan,
            'penyedia_beasiswa' => $request->penyedia_beasiswa,
            'jabatan_penjamin' => $request->jabatan_penjamin,
        ];

        if (auth()->user()->role == 'admin') {
            $admin = Admin::where('user_id', auth()->user()->id)->first();
            $data['admin_id'] = $admin->id;
        }

        if ($request->file('surat_jaminan_keuangan')){
            $extension  = $request->file('surat_jaminan_keuangan')->getClientOriginalExtension();
            $filename = $user->nim .'-financial-guarantee-letter-'.rand(0, 999). now()->timestamp . '.' . $extension;
            $path = $request->file('surat_jaminan_keuangan')->storeAs('public/files/pengajuan/surat_jaminan_keuangan', $filename);
            $path = str_replace('public', '/storage', $path);
            $data['surat_jaminan_keuangan'] = $path;
        }
        if ($request->file('surat_pernyataan')){
            $extension  = $request->file('surat_pernyataan')->getClientOriginalExtension();
            $filename = $user->nim .'-statement-letter-'.rand(0, 999). now()->timestamp . '.' . $extension;
            $path = $request->file('surat_pernyataan')->storeAs('public/files/pengajuan/surat_pernyataan', $filename);
            $path = str_replace('public', '/storage', $path);
            $data['surat_pernyataan'] = $path;
        }
        if ($request->file('surat_kesehatan')){
            $extension  = $request->file('surat_kesehatan')->getClientOriginalExtension();
            $filename = $user->nim .'-health-document-'.rand(0, 999). now()->timestamp . '.' . $extension;
            $path = $request->file('surat_kesehatan')->storeAs('public/files/pengajuan/surat_kesehatan', $filename);
            $path = str_replace('public', '/storage', $path);
            $data['surat_kesehatan'] = $path;
        }
        if ($request->file('letter_of_accept')){
            $extension  = $request->file('letter_of_accept')->getClientOriginalExtension();
            $filename = $user->nim .'-letter-of-accept-'.rand(0, 999). now()->timestamp . '.' . $extension;
            $path = $request->file('letter_of_accept')->storeAs('public/files/pengajuan/letter_of_accept', $filename);
            $path = str_replace('public', '/storage', $path);
            $data['letter_of_accept'] = $path;
        }
        if ($request->file('ijasah_terakhir')){
            $extension  = $request->file('ijasah_terakhir')->getClientOriginalExtension();
            $filename = $user->nim .'-latest-study-certificate-'.rand(0, 999). now()->timestamp . '.' . $extension;
            $path = $request->file('ijasah_terakhir')->storeAs('public/files/pengajuan/ijasah_terakhir', $filename);
            $path = str_replace('public', '/storage', $path);
            $data['ijasah_terakhir'] = $path;
        }
        if ($request->file('surat_ijin_belajar')){
            $extension  = $request->file('surat_ijin_belajar')->getClientOriginalExtension();
            $filename = $user->nim .'-study-permit-letter-'.rand(0, 999). now()->timestamp . '.' . $extension;
            $path = $request->file('surat_ijin_belajar')->storeAs('public/files/pengajuan/surat_ijin_belajar', $filename);
            $path = str_replace('public', '/storage', $path);
            $data['surat_ijin_belajar'] = $path;
        }
        if ($request->file('foto')){
            $extension  = $request->file('foto')->getClientOriginalExtension();
            $filename = $user->nim .'-photo-'.rand(0, 999). now()->timestamp . '.' . $extension;
            $path = $request->file('foto')->storeAs('public/files/pengajuan/foto', $filename);
            $path = str_replace('public', '/storage', $path);
            $data['foto'] = $path;
        }
        if ($request->input('status')){
            $data['status'] = $request->input('status');
        }
        if ($request->input('note')){
            $data['note'] = $request->input('note');
        }

        $pengajuan = Pengajuan::where('id', $id)->first();
        if($pengajuan->status == 'rejected' && auth()->user()->role != 'admin'){
            $data['status'] = 'checking';
        }

        if($request->input('status') == 'complete'){
            $data['tanggal_verifikasi'] = date('Y-m-d H:i:s');
        }

        if (auth()->user()->role == 'admin') {
            $admin = Admin::where('user_id', auth()->user()->id)->first();
            $user_change = $admin->nama_lengkap;
            $user_change_role = 'Admin';
        } else {
            $mahasiswa = Mahasiswa::where('user_id', auth()->user()->id)->first();
            $user_change = $mahasiswa->nama_lengkap;
            $user_change_role = 'Mahasiswa';
        }

        $pengajuan = Pengajuan::find($id);
        $this->createPengajuanLog([
            'pengajuan_id' => $pengajuan->id,
            'status_lama' => $pengajuan->status,
            'status_baru' => $data['status'],
            'tanggal' => date('Y-m-d H:i:s'),
            'user_change' => $user_change,
            'user_change_role' => $user_change_role,
            'note' => $request->input('note') ? $request->input('note') : '',
        ]);
        
        Pengajuan::where('id', $id)->update($data);
        
        Alert::success('Updated', 'Submission Data Updated Succesfuly!');
        if(auth()->user()->role == 'admin'){
            return redirect('submission');
        }else{
            return redirect('submission/create');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pengajuan  $pengajuan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pengajuan::destroy($id);
        return response()->json(['status' => 'Data Deleted Successfuly!']);
    }

    public function informasi(Request $request)
    {
        $user = User::where('id',auth()->user()->id)->with('mahasiswaDetail')->first();
        $pengajuan = Pengajuan::where('mahasiswa_id', $user->mahasiswaDetail->id)->with('mahasiswa.user')->first();
        $page = 'create';
        $admin = null;
        if ($pengajuan != null &&$pengajuan->count() > 0){
            $page = 'show';
            if (!empty($pengajuan->admin_id) && $pengajuan->admin_id != 0) {

                $admin = Admin::find($pengajuan->admin_id);
            }
        }
        return view("informasi.show", compact('user', 'page', 'pengajuan', 'admin'));
    }

    public function export(Request $request){
        return Excel::download(new ExportSubmission, 'submission-data.xlsx');
    }
}
