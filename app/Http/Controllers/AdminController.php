<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\User;
use Alert;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = Admin::all();
        return view("user-admin.index", compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("user-admin.insert");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            "username" => "required",
            "email" => "required|email",
            "password" => "required",
            "nama_lengkap" => "required",
            "no_telp" => "required",
            "jenis_kelamin" => "required",
            "agama" => "required",
        ];
        
        $msg = [
            'username.required' => 'Username is required',
            'email.required' => 'Email is required',
            'email.email' => 'Email format is incorrect',
            'password.required' => 'Password is required',
            'nama_lengkap.required' => 'Name Lengkap is required',
            'no_telp.required' => 'Phone Number is required',
            'jenis_kelamin.required' => 'Gender is required',
            'agama.required' => 'Religion is required',
        ];
        $request->validate($rules, $msg);
        
        $checkname = User::where("username", $request->username)->count();
        $checkemail = User::where("email", $request->email)->count();
        if($checkname > 0){
            Alert::error('Gagal', 'Username already exist!');
            return redirect('/admin/create');
        }
        if($checkemail > 0){
            Alert::error('Gagal', 'Email already exist!');
            return redirect('/admin/create');
        }
        
        $data = [
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'nim' => 0,
            'role' => 'admin'
        ];
        $user = User::create($data);

        
        $data = [
            'user_id' => $user->id,
            'nama_lengkap' => $request->nama_lengkap,
            'no_telp' => $request->no_telp,
            'jenis_kelamin' => $request->jenis_kelamin,
            'agama' => $request->agama,
        ];

        Admin::create($data);
        
        Alert::success('Saved', 'Admin Data Saved Succesfuly!');
        return redirect('admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        $admin = Admin::findOrFail($admin->id);
        return view("user-admin.show", compact("admin"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        $admin = Admin::findOrFail($admin->id);
        return view("user-admin.edit", compact("admin"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {

        $rules = [
            "username" => "required",
            "email" => "required|email",
            "nama_lengkap" => "required",
            "no_telp" => "required",
            "jenis_kelamin" => "required",
            "agama" => "required",
        ];
        
        $msg = [
            'username.required' => 'Username is required',
            'email.required' => 'Email is required',
            'email.email' => 'Email format is incorrect',
            'nama_lengkap.required' => 'Name Lengkap is required',
            'no_telp.required' => 'Phone Number is required',
            'jenis_kelamin.required' => 'Gender is required',
            'agama.required' => 'Religion is required',
        ];
        $request->validate($rules, $msg);
        
        $user = User::where('id', $admin->user->id)->first();
        if ($request->password == "") {
            $password = $user->password;
        }else{
            $password = bcrypt($request->password);
        }

        $checkname = User::where([
                            ["id", "!=", $user->id],
                            ["username", $request->username]
                            ])->count();
        $checkemail = User::where([
                            ["id", "!=", $user->id],
                            ["email", $request->email]
                            ])->count();
                            
        if($checkname > 0){
            Alert::error('Gagal', 'Username already exist!');
            return redirect("admin/".$admin->id."/edit");
        }
        if($checkemail > 0){
            Alert::error('Gagal', 'Email already exist!');
            return redirect("admin/".$admin->id."/edit");
        }

        $data = [
            'username' => $request->username,
            'email' => $request->email,
            'password' => $password,
            'nim' => $request->nim,
        ];
        $user->update($data);

        $data = [
            'user_id' => $user->id,
            'nama_lengkap' => $request->nama_lengkap,
            'no_telp' => $request->no_telp,
            'jenis_kelamin' => $request->jenis_kelamin,
            'agama' => $request->agama,
        ];

        Admin::where('user_id',$user->id)->update($data);
                
        Alert::success('Updated', 'Admin Data Updated Succesfuly!');
        return redirect('/admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        Admin::destroy($admin->id);
        return response()->json(['status' => 'Data Deleted Successfuly!']);
    }
}
