<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Mahasiswa;
use Illuminate\Http\Request;
use Alert;
use App\Exports\ExportStudent;
use Maatwebsite\Excel\Facades\Excel;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('mahasiswaDetail')->where('role', '!=', 'admin')->get();
        return view("admin.index", compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.insert");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            "username" => "required",
            "email" => "required|email",
            "password" => "required",
            "nama_lengkap" => "required",
            "no_telp" => "required",
            "jenis_kelamin" => "required",
            "agama" => "required",
            "nama_ayah" => "required",
            "nama_ibu" => "required",
        ];
        
        $msg = [
            'username.required' => 'Username is required',
            'nim.required' => 'NIM is required',
            'email.required' => 'Email is required',
            'email.email' => 'Email format is incorrect',
            'password.required' => 'Password is required',
            'nama_lengkap.required' => 'Name Lengkap is required',
            'no_telp.required' => 'Phone Number is required',
            'jenis_kelamin.required' => 'Gender is required',
            'agama.required' => 'Religion is required',
            'nama_ayah.required' => 'Father Name is required',
            'nama_ibu.required' => 'Mother Name is required',
        ];
        $request->validate($rules, $msg);
        
        $checkname = User::where("username", $request->username)->count();
        $checkemail = User::where("email", $request->email)->count();
        if($checkname > 0){
            Alert::error('Gagal', 'Username already exist!');
            return redirect('/user/create');
        }
        if($checkemail > 0){
            Alert::error('Gagal', 'Email already exist!');
            return redirect('/user/create');
        }
        
        $data = [
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'nim' => $request->nim,
            'role' => 'user'
        ];
        $user = User::create($data);

        
        $data = [
            'user_id' => $user->id,
            'nama_lengkap' => $request->nama_lengkap,
            'no_telp' => $request->no_telp,
            'jenis_kelamin' => $request->jenis_kelamin,
            'agama' => $request->agama,
            'nama_ayah' => $request->nama_ayah,
            'nama_ibu' => $request->nama_ibu,
        ];

        Mahasiswa::create($data);
        
        Alert::success('Saved', 'Foreign Student Data Saved Succesfuly!');
        return redirect('user');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $user = User::with('mahasiswaDetail')->findOrFail($user->id);
        return view("admin.show", compact("user"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $user = User::with('mahasiswaDetail')->findOrFail($user->id);
        return view("admin.edit", compact("user"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        $rules = [
            "username" => "required",
            "nim" => "required",
            "email" => "required|email",
            "nama_lengkap" => "required",
            "no_telp" => "required",
            "jenis_kelamin" => "required",
            "agama" => "required",
            "nama_ayah" => "required",
            "nama_ibu" => "required",
        ];
        
        $msg = [
            'username.required' => 'Username is required',
            'nim.required' => 'NIM is required',
            'email.required' => 'Email is required',
            'email.email' => 'Email format is incorrect',
            'nama_lengkap.required' => 'Name Lengkap is required',
            'no_telp.required' => 'Phone Number is required',
            'jenis_kelamin.required' => 'Gender is required',
            'agama.required' => 'Religion is required',
            'nama_ayah.required' => 'Father Name is required',
            'nama_ibu.required' => 'Mother Name is required',
        ];
        $request->validate($rules, $msg);
        
        $user = User::where('id', $user->id)->first();
        if ($request->password == "") {
            $password = $user->password;
        }else{
            $password = bcrypt($request->password);
        }

        $checkname = User::where([
                            ["id", "!=", $user->id],
                            ["username", $request->username]
                            ])->count();
        $checkemail = User::where([
                            ["id", "!=", $user->id],
                            ["email", $request->email]
                            ])->count();
                            
        if($checkname > 0){
            Alert::error('Gagal', 'Username already exist!');
            return redirect("user/".$user->id."/edit");
        }
        if($checkemail > 0){
            Alert::error('Gagal', 'Email already exist!');
            return redirect("user/".$user->id."/edit");
        }

        $data = [
            'username' => $request->username,
            'email' => $request->email,
            'password' => $password,
            'nim' => $request->nim,
        ];
        $user->update($data);

        $data = [
            'user_id' => $user->id,
            'nama_lengkap' => $request->nama_lengkap,
            'no_telp' => $request->no_telp,
            'jenis_kelamin' => $request->jenis_kelamin,
            'agama' => $request->agama,
            'nama_ayah' => $request->nama_ayah,
            'nama_ibu' => $request->nama_ibu,
        ];

        Mahasiswa::where('user_id',$user->id)->update($data);
                
        Alert::success('Updated', 'Foreign Student Data Updated Succesfuly!');
        return redirect('/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        User::destroy($user->id);
        return response()->json(['status' => 'Data Deleted Successfuly!']);
    }

    public function export(Request $request){
        return Excel::download(new ExportStudent, 'foreign-student-data.xlsx');
    }
}