<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Mahasiswa;
use Illuminate\Support\Facades\Auth;
use Alert;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResetPasswordEmail;

class AuthController extends Controller
{

    public function login()
    {
        return view('auth.index');
    }

    public function postLogin(Request $request)
    {
        if(Auth::attempt(['username'=>$request->username, 'password'=>$request->password, 'role'=>'admin']))
        {
            return redirect('/user');
        }
        if(Auth::attempt(['username'=>$request->username, 'password'=>$request->password, 'role'=>'user']))
        {
            return redirect('/home');
        }
        else
        {
            return redirect('/')->with('status', 'Username & Password Salah!');
        }
    }

    function logout(){
        Auth::logout();
        return redirect('/');
    }

    public function register()
    {
        return view('auth.register');
    }

    public function postRegister(Request $request)
    {
        $rules = [
            "username" => "required",
            "email" => "required|email",
            "password" => "required",
            "nama_lengkap" => "required",
            "no_telp" => "required",
            "jenis_kelamin" => "required",
            "agama" => "required",
            "nama_ayah" => "required",
            "nama_ibu" => "required",
        ];
        
        $msg = [
            'username.required' => 'Username is required',
            'nim.required' => 'NIM is required',
            'email.required' => 'Email is required',
            'email.email' => 'Email format is incorrect',
            'password.required' => 'Password is required',
            'nama_lengkap.required' => 'Name Lengkap is required',
            'no_telp.required' => 'Phone Number is required',
            'jenis_kelamin.required' => 'Gender is required',
            'agama.required' => 'Religion is required',
            'nama_ayah.required' => 'Father Name is required',
            'nama_ibu.required' => 'Mother Name is required',
        ];
        $request->validate($rules, $msg);
        
        $checkname = User::where("username", $request->username)->count();
        $checkemail = User::where("email", $request->email)->count();
        if($checkname > 0){
            Alert::error('Gagal', 'Username already exist!');
            return redirect('/user/create');
        }
        if($checkemail > 0){
            Alert::error('Gagal', 'Email already exist!');
            return redirect('/user/create');
        }
        
        $data = [
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'nim' => $request->nim,
            'role' => 'user'
        ];
        $user = User::create($data);

        
        $data = [
            'user_id' => $user->id,
            'nama_lengkap' => $request->nama_lengkap,
            'no_telp' => $request->no_telp,
            'jenis_kelamin' => $request->jenis_kelamin,
            'agama' => $request->agama,
            'nama_ayah' => $request->nama_ayah,
            'nama_ibu' => $request->nama_ibu,
        ];

        Mahasiswa::create($data);
        
        return redirect('/')->with('register', 'Registration Successful.');
    }

    public function forgotPassword()
    {
        return view('auth.forgot_password');
    }

    public function postForgotPassword(Request $request)
    {
        $email = $request->email;
        $user = User::where('email', '=', $email);
        if($user->count() == 0){
            return redirect('/forgot-password')->with('failed', 'No email matches, ensure you type a correct email address :)');
        }
        

        $reset_token =rand(1111111111,9999999999);
        $user = $user->first();
        $data = [];
        $data['username'] = $user->username;
        // $data['url'] = env('APP_URL');
        $data['url'] = url('/');
        $data['token'] = $reset_token;

        $user->reset_token = $reset_token;
        $user->save();

        Mail::to($email)->send(new ResetPasswordEmail($data));
        

        return redirect('/forgot-password')->with('success', 'Please check your email to reset your password :D');
    }

    public function resetPassword($token)
    {
        $user = User::where('reset_token', '=', $token);
        if($user->count() == 0){
            return redirect('/')->with('status', 'Token is invalid.');
        }
        return view('auth.reset_password', compact('token'));
    }

    public function postResetPassword(Request $request)
    {
        $rules = [
            'password' => 'required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'required'
        ];
        
        $msg = [
            'password.required' => 'Password is required',
            'password_confirmation.required' => 'Password Confirmation is required',
        ];
        $request->validate($rules, $msg);

        $user = User::where('reset_token', '=', $request->token)->first();
        $user->password = bcrypt($request->password);
        $user->reset_token = null;
        $user->save();

        return redirect('/')->with('register', 'Your password succesfuly changed.');
    }
}