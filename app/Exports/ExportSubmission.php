<?php

namespace App\Exports;

use App\Models\Pengajuan;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;

class ExportSubmission implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $no;
    public function collection()
    {
        return Pengajuan::with('mahasiswa.user')->get();
    }

    public function map($data): array
    {
        return [
            $this->no += 1,
            $data->mahasiswa->nama_lengkap,
            $data->mahasiswa->jenis_kelamin,
            $data->mahasiswa->user->email,
            $data->mahasiswa->no_telp,
            $data->tempat_lahir,
            $data->tanggal_lahir,
            $data->kebangsaan,
            $data->alamat_asal,
            $data->kota_asal,
            $data->provinsi_asal,
            $data->negara_asal,
            $data->kode_pos_asal,
            $data->alamat_tinggal,
            $data->kota_tinggal,
            $data->provinsi_tinggal,
            $data->kode_pos_tinggal,
            $data->jenjang,
            $data->angkatan,
            $data->prodi,
            $data->mulai_ijin_belajar,
            $data->lama_ijin_belajar,
            $data->no_paspor,
            $data->tgl_pengajuan_paspor,
            $data->tgl_berakhir,
            $data->jenis_pengadaan,
            $data->penyedia_beasiswa,
            $data->jabatan_penjamin,
            $data->status,
            $data->note ?? '-',
        ];
    }

    public function headings(): array
    {
        return [
            "No",
            "Name",
            "Gender",
            "Email",
            "Phone Number",
            "Place of Birth",
            "Date of Birth",
            "Nationality",
            "Original Address",
            "Original City",
            "Original State",
            "Original Country",
            "Original Postal Code",
            "Address",
            "City",
            "State",
            "Postal Code",
            "Education Level",
            "Level of Study",
            "Study Program",
            "Start Learning",
            "Long Study Permit",
            "Passport Number",
            "Date of Issue",
            "Date of Expired",
            "Type of Funding",
            "Scholarship Provider",
            "Guarantor Position",
            "Status",
            "Note"


        ];
    }
}
