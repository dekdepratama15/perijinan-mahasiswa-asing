<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;

class ExportStudent implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $no;
    public function collection()
    {
        return User::with('mahasiswaDetail')->where('role', '!=', 'admin')->get();;
    }

    public function map($data): array
    {
        return [
            $this->no += 1,
            $data->username,
            $data->nim,
            $data->email,
            $data->mahasiswaDetail->nama_lengkap,
            $data->mahasiswaDetail->no_telp,
            $data->mahasiswaDetail->jenis_kelamin,
            $data->mahasiswaDetail->agama,
            $data->mahasiswaDetail->nama_ayah,
            $data->mahasiswaDetail->nama_ibu,
        ];
    }

    public function headings(): array
    {
        return ["No", "Username", "NIM", "Email", "Name", "Phone Number", "Gender", "Religion", "Father Name", "Mother Name"];
    }
}
