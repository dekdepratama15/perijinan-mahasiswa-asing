
<div class="row p-3">
    <div class="col-6">
        <div class="form-group">
            <label>Passport Number <sup style="color: red">*</sup></label>
            <input type="text" class="form-control @error('no_paspor') is-invalid @enderror" name="no_paspor" id="no_paspor" value="{{ old('no_paspor') }}" placeholder="Passport Number" autocomplete="off">
                @error('no_paspor')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div> 
                @enderror
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Date of Issue <sup style="color: red">*</sup></label>
            <input type="date" class="form-control @error('tgl_pengajuan_paspor') is-invalid @enderror" name="tgl_pengajuan_paspor" id="tgl_pengajuan_paspor" value="{{ old('tgl_pengajuan_paspor') }}" placeholder="Date of Issue" autocomplete="off">
                @error('tgl_pengajuan_paspor')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div> 
                @enderror
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Date of Expired <sup style="color: red">*</sup></label>
            <input type="date" class="form-control @error('tgl_berakhir') is-invalid @enderror" name="tgl_berakhir" id="tgl_berakhir" value="{{ old('tgl_berakhir') }}" placeholder="Date of Expired" autocomplete="off">
                @error('tgl_berakhir')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div> 
                @enderror
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Type of Funding <sup style="color: red">*</sup></label>
            <select name="jenis_pengadaan" class="form-control  @error('jenis_pengadaan') is-invalid @enderror" id="jenis_pengadaan">
                <option value="" selected disabled>Select Option</option>
                <option value="Independent Fees">Independent Fees</option>
                <option value="Scholarships">Scholarships</option>
                <option value="Others">Others</option>
            </select>
                @error('jenis_pengadaan')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div> 
                @enderror
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Scholarship Provider <sup style="color: red">*</sup></label>
            <input type="text" class="form-control @error('penyedia_beasiswa') is-invalid @enderror" name="penyedia_beasiswa" id="penyedia_beasiswa" value="{{ old('penyedia_beasiswa') }}" placeholder="Scholarship Provider" autocomplete="off">
                @error('penyedia_beasiswa')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div> 
                @enderror
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Guarantor Position <sup style="color: red">*</sup></label>
            <input type="text" class="form-control @error('jabatan_penjamin') is-invalid @enderror" name="jabatan_penjamin" id="jabatan_penjamin" value="{{ old('jabatan_penjamin') }}" placeholder="Guarantor Position" autocomplete="off">
                @error('jabatan_penjamin')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div> 
                @enderror
        </div>
    </div>
</div>
<button type="button" id="prevToStudy" class="btn btn-secondary mt-5 float-left">Previous</button>
<button type="button" id="nextToDocument" class="btn btn-secondary ml-2 mt-5 float-right">Next</button>
