<div class="card">
    <div class="card-header">
        <label for="">Study Information</label>
    </div>
    <div class="card-body p-3">
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>Education Level <sup style="color: red">*</sup></label>
                    <select name="jenjang" class="form-control  @error('jenjang') is-invalid @enderror" id="jenjang">
                        <option value="" selected disabled>Select Option</option>
                        <option value="D3" @if (old('jenjang') == 'D3') selected @endif>D3</option>
                        <option value="S1" @if (old('jenjang') == 'S1') selected @endif>S1</option>
                        <option value="S2" @if (old('jenjang') == 'S2') selected @endif>S2</option>
                    </select>
                        @error('jenjang')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Level of Study <sup style="color: red">*</sup></label>
                    <input type="number" class="form-control @error('angkatan') is-invalid @enderror" name="angkatan" id="angkatan" value="{{ old('angkatan') }}" placeholder="Level of Study" autocomplete="off">
                        @error('angkatan')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Study Program <sup style="color: red">*</sup></label>
                    <select name="prodi" class="form-control  @error('prodi') is-invalid @enderror" id="prodi">
                        <option value="" selected disabled>Select Option</option>
                        <option value="Sistem Informasi" @if (old('prodi') == 'Sistem Informasi') selected @endif>Sistem Informasi</option>
                        <option value="Sistem Komputer" @if (old('prodi') == 'Sistem Komputer') selected @endif>Sistem Komputer</option>
                        <option value="Teknologi Informasi" @if (old('prodi') == 'Teknologi Informasi') selected @endif>Teknologi Informasi</option>
                        <option value="Bisnis Digital" @if (old('prodi') == 'Bisnis Digital') selected @endif>Bisnis Digital</option>
                        <option value="Manajemen Informatika" @if (old('prodi') == 'Manajemen Informatika') selected @endif>Manajemen Informatika</option>
                        <option value="Internasional Dual Degree" @if (old('prodi') == 'Internasional Dual Degree') selected @endif>Internasional Dual Degree</option>
                    </select>
                        @error('prodi')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <label for="">Study Permit Period</label>
    </div>
    <div class="card-body p-3">
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>Start Learning <sup style="color: red">*</sup></label>
                    <input type="date" class="form-control @error('mulai_ijin_belajar') is-invalid @enderror" name="mulai_ijin_belajar" id="mulai_ijin_belajar" value="{{ old('mulai_ijin_belajar') }}" placeholder="Start Learning" autocomplete="off">
                        @error('mulai_ijin_belajar')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Long Study Permit <sup style="color: red">*</sup></label>
                    <input type="date" class="form-control @error('lama_ijin_belajar') is-invalid @enderror" name="lama_ijin_belajar" id="lama_ijin_belajar" value="{{ old('lama_ijin_belajar') }}" placeholder="Long Study Permit" autocomplete="off">
                        @error('lama_ijin_belajar')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
        </div>
    </div>
</div>
<button type="button" id="prevToIdentity" class="btn btn-secondary mt-5 float-left">Previous</button>
<button type="button" id="nextToSupporting" class="btn btn-secondary ml-2 mt-5 float-right">Next</button>