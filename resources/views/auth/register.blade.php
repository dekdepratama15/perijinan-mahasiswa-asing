<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SIPIBE</title>
<link rel="icon" type="image/gif/png/svg" href="{{ asset("img/logo.png") }}">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css')}}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css')}}">
</head>
<body class="hold-transition login-page">
<div class="login-box" style="width: 800px!important">
<!-- /.login-logo -->
    <div class="card shadow mb-4">
        <div class="card-body card-primary card-outline">
            <h2 class="text-center mb-5">Sign Up</h2>
            <form method="POST" action="/register" id="theForm" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Username <sup style="color: red">*</sup></label>
                            <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" id="username" value="{{ old('username') }}" placeholder="Username" autocomplete="off">
                                @error('username')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div> 
                                @enderror
                        </div>
                        <div class="form-group">
                            <label>Name<sup style="color: red">*</sup></label>
                            <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap" id="nama_lengkap" value="{{ old('nama_lengkap') }}" placeholder="Name" autocomplete="off">
                                @error('nama_lengkap')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div> 
                                @enderror
                        </div>
                        <div class="form-group">
                            <label>Gender <sup style="color: red">*</sup></label>
                            <select name="jenis_kelamin" class="form-control  @error('jenis_kelamin') is-invalid @enderror" id="jenis_kelamin">
                                <option value="" selected disabled>Select Option</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                                @error('jenis_kelamin')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div> 
                                @enderror
                        </div>
                        <div class="form-group">
                            <label>Father Name <sup style="color: red">*</sup></label>
                            <input type="text" class="form-control @error('nama_ayah') is-invalid @enderror" name="nama_ayah" id="nama_ayah" value="{{ old('nama_ayah') }}" placeholder="Father Name" autocomplete="off">
                                @error('nama_ayah')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div> 
                                @enderror
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" value="{{ old('password') }}" placeholder="Password" autocomplete="off">
                                @error('password')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div> 
                                @enderror
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label>NIM</label>
                            <input type="text" class="form-control @error('nim') is-invalid @enderror" name="nim" id="nim" value="{{ old('nim') }}" placeholder="NIM" autocomplete="off">
                                @error('nim')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div> 
                                @enderror
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" class="form-control @error('email') is-invalid @enderror" name="email" id="email" value="{{ old('email') }}" placeholder="Email" autocomplete="off">
                                @error('email')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div> 
                                @enderror
                        </div>
                        <div class="form-group">
                            <label>Phone Number <sup style="color: red">*</sup></label>
                            <input type="text" class="form-control @error('no_telp') is-invalid @enderror" name="no_telp" id="no_telp" value="{{ old('no_telp') }}" placeholder="Phone Number" autocomplete="off">
                                @error('no_telp')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div> 
                                @enderror
                        </div>
                        <div class="form-group">
                            <label>Religion <sup style="color: red">*</sup></label>
                            <select name="agama" class="form-control  @error('agama') is-invalid @enderror" id="agama">
                                <option value="" selected disabled>Select Option</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Islam">Islam</option>
                                <option value="Buddha">Buddha</option>
                                <option value="Kristen Katolik">Kristen Katolik</option>
                                <option value="Kristen Protestan">Kristen Protestan</option>
                                <option value="Konghucu">Konghucu</option>
                            </select>
                                @error('agama')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div> 
                                @enderror
                        </div>
                        <div class="form-group">
                            <label>Mother Name <sup style="color: red">*</sup></label>
                            <input type="text" class="form-control @error('nama_ibu') is-invalid @enderror" name="nama_ibu" id="nama_ibu" value="{{ old('nama_ibu') }}" placeholder="Mother Name" autocomplete="off">
                                @error('nama_ibu')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div> 
                                @enderror
                        </div>
                    </div>
                </div>
                <a class="btn btn-outline-danger mt-3" href="{{ url('/') }}">Back</a>
                <button type="submit" id="btnSubmit" class="btn btn-primary ml-2 mt-3">Submit</button>
            </form>
        </div>
    </div>
    <!-- /.card -->
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<script>
    $(document).ready(function() {
        window.setTimeout(function() {
            $("#info").fadeTo(500, 0).slideUp(500, function() {
                $(this).remove();
            });
        }, 3000);
    });
</script>
</body>
</html>
