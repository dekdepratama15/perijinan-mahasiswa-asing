<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SIPIBE</title>
<link rel="icon" type="image/gif/png/svg" href="{{ asset("img/logo.png") }}">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css')}}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css')}}">
</head>
<body class="hold-transition login-page">
<div class="login-box">
<!-- /.login-logo -->
    <div class="card card-outline card-primary">
        <div class="card-header text-center">
            <img height="75px" src="{{ asset("img/logo.png") }}" alt="logo_pln">
            <h2 class="mt-3">SIPIBE</h2>
        </div>
        <div class="card-body">
        @if (session('failed'))
        <div id="info" class="alert alert-danger">
            {{ session('failed') }}
        </div>
        @endif
        @if (session('success'))
        <div id="info" class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        <form action="{{ url("/postForgotPassword") }}" method="post">
            @csrf
            <p>Write your email account to send reset password verification link.</p>
            <div class="input-group mb-3">
            <input type="email" class="form-control" name="email" placeholder="Email" autocomplete="off">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-user"></span>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary btn-block">Submit</button>
            <a href="{{ url("/") }}" class="btn btn-outline-danger btn-block mb-2">Back</a>
        </form>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<script>
    $(document).ready(function() {
        window.setTimeout(function() {
            $("#info").fadeTo(500, 0).slideUp(500, function() {
                $(this).remove();
            });
        }, 3000);
    });
</script>
</body>
</html>
