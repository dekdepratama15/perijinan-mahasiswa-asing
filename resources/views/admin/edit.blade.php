@extends('templates.main')
@section('title')
    Foreign Student
@endsection
@section('page')
    Edit Foreign Student
@endsection
@section('content')
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-body card-primary card-outline">
        <form method="POST" action="/user/{{ $user->id }}" id="theForm" enctype="multipart/form-data">
            @method('patch')
            @csrf
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label>Username <sup style="color: red">*</sup></label>
                        <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" id="username" value="{{ old('username', $user->username) }}" autocomplete="off">
                            @error('username')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div> 
                            @enderror
                    </div>
                    <div class="form-group">
                        <label>Name <sup style="color: red">*</sup></label>
                        <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap" id="nama_lengkap" value="{{ old('nama_lengkap', $user->mahasiswaDetail->nama_lengkap) }}" placeholder="Name" autocomplete="off">
                            @error('nama_lengkap')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div> 
                            @enderror
                    </div>
                    <div class="form-group">
                        <label>Gender <sup style="color: red">*</sup></label>
                        <select name="jenis_kelamin" class="form-control  @error('jenis_kelamin') is-invalid @enderror" id="jenis_kelamin">
                            <option value="" selected disabled>Select Option</option>
                            <option value="Male" @if ($user->mahasiswaDetail->jenis_kelamin == 'Male') selected @endif>Male</option>
                            <option value="Female" @if ($user->mahasiswaDetail->jenis_kelamin == 'Female') selected @endif>Female</option>
                        </select>
                            @error('jenis_kelamin')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div> 
                            @enderror
                    </div>
                    <div class="form-group">
                        <label>Father Name <sup style="color: red">*</sup></label>
                        <input type="text" class="form-control @error('nama_ayah') is-invalid @enderror" name="nama_ayah" id="nama_ayah" value="{{ old('nama_ayah', $user->mahasiswaDetail->nama_ayah) }}" placeholder="Father Name" autocomplete="off">
                            @error('nama_ayah')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div> 
                            @enderror
                    </div>
                    {{-- <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" value="{{ old('password') }}" placeholder="Password" autocomplete="off">
                            @error('password')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div> 
                            @enderror
                    </div> --}}
                    <div class="form-group">
                        <label>Mother Name <sup style="color: red">*</sup></label>
                        <input type="text" class="form-control @error('nama_ibu') is-invalid @enderror" name="nama_ibu" id="nama_ibu" value="{{ old('nama_ibu', $user->mahasiswaDetail->nama_ibu) }}" placeholder="Mother Name" autocomplete="off">
                            @error('nama_ibu')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div> 
                            @enderror
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>NIM</label>
                        <input type="text" class="form-control @error('nim') is-invalid @enderror" name="nim" id="nim" value="{{ old('nim', $user->nim) }}" placeholder="NIM" autocomplete="off">
                            @error('nim')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div> 
                            @enderror
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" class="form-control @error('email') is-invalid @enderror" name="email" id="email" value="{{ old('email', $user->email) }}" placeholder="Email" autocomplete="off">
                            @error('email')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div> 
                            @enderror
                    </div>
                    <div class="form-group">
                        <label>Phone Number <sup style="color: red">*</sup></label>
                        <input type="text" class="form-control @error('no_telp') is-invalid @enderror" name="no_telp" id="no_telp" value="{{ old('no_telp', $user->mahasiswaDetail->no_telp) }}" placeholder="Phone Number" autocomplete="off">
                            @error('no_telp')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div> 
                            @enderror
                    </div>
                    <div class="form-group">
                        <label>Religion <sup style="color: red">*</sup></label>
                        <select name="agama" class="form-control  @error('agama') is-invalid @enderror" id="agama">
                            <option value="" selected disabled>Select Option</option>
                            <option value="Hindu" @if ($user->mahasiswaDetail->agama == 'Hindu') selected @endif>Hindu</option>
                            <option value="Islam" @if ($user->mahasiswaDetail->agama == 'Islam') selected @endif>Islam</option>
                            <option value="Buddha" @if ($user->mahasiswaDetail->agama == 'Buddha') selected @endif>Buddha</option>
                            <option value="Kristen Katolik" @if ($user->mahasiswaDetail->agama == 'Kristen Katolik') selected @endif>Kristen Katolik</option>
                            <option value="Kristen Protestan" @if ($user->mahasiswaDetail->agama == 'Kristen Protestan') selected @endif>Kristen Protestan</option>
                            <option value="Konghucu" @if ($user->mahasiswaDetail->agama == 'Konghucu') selected @endif>Konghucu</option>
                        </select>
                            @error('agama')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div> 
                            @enderror
                    </div>
                </div>
            </div>
            
            <a class="btn btn-outline-danger mt-3" href="{{ url('/user') }}">Back</a>
            <button type="submit" id="btnSubmit" class="btn btn-primary ml-2 mt-3">Submit</button>
        </form>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $('.custom-file-input').change(function (e) {
        if (e.target.files.length) {
            $(this).next('.custom-file-label').html(e.target.files[0].name);
        }
    });
</script>
@endsection