@extends('templates.main')
@section('title')
    Foreign Student
@endsection
@section('page')
    Foreign Student Details
@endsection
@section('content')
<div class="card card-primary card-outline shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            {{-- <a class="btn btn-primary mb-3" href="{{ url('transaction/create') }}"><i
                class="fas fa-plus mr-2"></i>Tambah Data</a> --}}
            <table class="table table-striped" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="50%">
                        <b>NIM</b>
                    </td>
                    <td>
                        {{ $user->nim }}
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                        <b>Username</b>
                    </td>
                    <td>
                        {{ $user->username }}
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                        <b>Email</b>
                    </td>
                    <td>
                        {{ $user->email }}
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                        <b>Name</b>
                    </td>
                    <td>
                        {{ $user->mahasiswaDetail->nama_lengkap }}
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                        <b>Phone Number</b>
                    </td>
                    <td>
                        {{ $user->mahasiswaDetail->no_telp }}
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                        <b>Gender</b>
                    </td>
                    <td>
                        {{ $user->mahasiswaDetail->jenis_kelamin }}
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                        <b>Religion</b>
                    </td>
                    <td>
                        {{ $user->mahasiswaDetail->agama }}
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                        <b>Father Name</b>
                    </td>
                    <td>
                        {{ $user->mahasiswaDetail->nama_ayah }}
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                        <b>Mother Name</b>
                    </td>
                    <td>
                        {{ $user->mahasiswaDetail->nama_ibu }}
                    </td>
                </tr>
            </table>
            
            <a class="btn btn-outline-danger mt-3" href="{{ url('/user') }}">Back</a>
        </div>
    </div>
</div>
@endsection
@section('scripts')

@endsection

