@extends('templates.main')
@section('title')
    Submission
@endsection
@section('page')
    @if ($page == 'show')
        Details Submission
    @elseif ($page == 'edit')
        Edit Submission
    @elseif ($page == 'create')
        Add New Submission
    @endif
@endsection
@section('content')
<!-- DataTales Example -->
<div class="card">
    <div class="card-body">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="timeline-steps aos-init aos-animate" data-aos="fade-up">
                        <div class="timeline-step">
                            <div class="timeline-content" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2003">
                                <div class="inner-circle" @if (!isset($pengajuan)) style="background-color: #3b82f6" @endif></div>
                                <p class="h6 mt-3 mb-1">Not Submitted</p>
                                <p class="h6 text-muted mb-0 mb-lg-0">Please make a submission by filling in the form below.</p>
                            </div>
                        </div>
                        <div class="timeline-step">
                            <div class="timeline-content" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2004">
                                <div class="inner-circle" @if (isset($pengajuan->status)) @if ($pengajuan->status == 'checking') style="background-color: #3b82f6" @endif @endif></div>
                                <p class="h6 mt-3 mb-1">Checking</p>
                                <p class="h6 text-muted mb-0 mb-lg-0">Your application has been successful, please wait for the checking process from student affairs.</p>
                            </div>
                        </div>
                        <div class="timeline-step">
                            <div class="timeline-content" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2005">
                                <div class="inner-circle" @if (isset($pengajuan->status)) @if ($pengajuan->status == 'rejected') style="background-color: #3b82f6" @endif @endif></div>
                                <p class="h6 mt-3 mb-1">Rejected</p>
                                <p class="h6 text-muted mb-0 mb-lg-0">Your application has been rejected, please read the student affairs provisions and re-submit.</p>
                            </div>
                        </div>
                        <div class="timeline-step">
                            <div class="timeline-content" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2010">
                                <div class="inner-circle" @if (isset($pengajuan->status)) @if ($pengajuan->status == 'complete') style="background-color: #3b82f6" @endif @endif></div>
                                <p class="h6 mt-3 mb-1">Complete</p>
                                <p class="h6 text-muted mb-0 mb-lg-0">Your submission is complete.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card shadow mb-4">
    <div class="card-body card-primary card-outline">
        <form method="POST" 
        @if ($page != 'edit') action="{{ route('submission.store') }}" @else action="{{ route('submission.update', $pengajuan->id) }}" @endif
        id="theForm" enctype="multipart/form-data">
            @csrf
            @if ($page == 'edit')
                @method('PUT')
            @endif
            @if ($page == 'show' || ($page == 'edit' && auth()->user()->role == 'admin'))
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="identity-tab" data-toggle="tab" href="#identity" role="tab"
                        aria-controls="identity" aria-selected="true">Identity Information</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="study-tab" data-toggle="tab" href="#study" role="tab"
                        aria-controls="study" aria-selected="false">Study Information</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="supporting-tab" data-toggle="tab" href="#supporting" role="tab"
                        aria-controls="supporting" aria-selected="false">Supporting Information</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="document-tab" data-toggle="tab" href="#document" role="tab"
                        aria-controls="document" aria-selected="true">Supporting Document</a>
                </li>
            </ul>
            @endif
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active border border-top-0 rounded-bottom p-3" id="identity"
                    role="tabpanel" aria-labelledby="identity-tab">
                    @if ($page == 'show')
                        @include(
                            'pengajuan-form-detail.identitas'
                        )
                    @elseif ($page == 'edit')
                        @include(
                            'pengajuan-form-edit.identitas'
                        )
                    @elseif ($page == 'create')
                        @include(
                            'pengajuan-form.identitas',
                            ['user'=>$user]
                        )
                    @endif
                </div>
                <div class="tab-pane fade border border-top-0 rounded-bottom p-3" id="study" role="tabpanel"
                    aria-labelledby="study-tab">
                    @if ($page == 'show')
                        @include(
                            'pengajuan-form-detail.studi'
                        )
                    @elseif ($page == 'edit')
                        @include(
                            'pengajuan-form-edit.studi'
                        )
                    @elseif ($page == 'create')
                        @include(
                            'pengajuan-form.studi'
                        )
                    @endif
                </div>
                <div class="tab-pane fade border border-top-0 rounded-bottom p-3" id="supporting" role="tabpanel"
                    aria-labelledby="supporting-tab">
                    @if ($page == 'show')
                        @include(
                            'pengajuan-form-detail.tambahan'
                        )
                    @elseif ($page == 'edit')
                        @include(
                            'pengajuan-form-edit.tambahan'
                        )
                    @elseif ($page == 'create')
                        @include(
                            'pengajuan-form.tambahan'
                        )
                    @endif
                </div> 
                <div class="tab-pane fade border border-top-0 rounded-bottom p-3" id="document" role="tabpanel"
                    aria-labelledby="document-tab">
                    @if ($page == 'show')
                        @include(
                            'pengajuan-form-detail.dokumen'
                        )
                    @elseif ($page == 'edit')
                        @include(
                            'pengajuan-form-edit.dokumen'
                        )
                    @elseif ($page == 'create')
                        @include(
                            'pengajuan-form.dokumen'
                        )
                    @endif
                </div>
            </div>
            
            @if (auth()->user()->role == 'admin')
                <a class="btn btn-outline-danger mt-3" href="{{ url('/submission') }}">Back</a>
            @endif
            @if (auth()->user()->role == 'admin' && $page == 'edit')
                <button type="submit" id="btnSubmit" class="btn btn-primary ml-2 mt-3">Submit</button>
            @endif
            @if ( isset($pengajuan->status) && auth()->user()->role == 'user') 
                @if ($pengajuan->status == 'rejected' && $page == 'show') 
                    <a href="/submission/{{ $pengajuan->id }}/edit" class="btn btn-warning btn-width ml-2 mt-3" data-toggle="tooltip" data-placement="top" title="Edit">Edit Data</a>
                @endif 
            @endif
        </form>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $('.custom-file-input').change(function (e) {
        if (e.target.files.length) {
            $(this).next('.custom-file-label').html(e.target.files[0].name);
        }
    });
</script>
<script>
    $(document).ready(function () {
        if($('.status-select').val() == 'complete') {
            $('.note-input').attr('readonly', true);
            $('.permit-input').attr('readonly', false);
        }
    });

    let page = '{{ $page }}';
    let role = '{{ auth()->user()->role }}';
    let status = '{{ $pengajuan != null ? $pengajuan->status : '' }}';
    if (page == 'show') {
        $('.form-control').attr('readonly', true);
        $('#btnSubmit').hide();
    } 
    if (page == 'edit' && role == 'admin') {
        $('.form-control').attr('readonly', true);
        $('.status-select').attr('readonly', false);
        $('.note-input').attr('readonly', false);
        if('{{ isset($pengajuan->status) }}'){
            if('{{ $pengajuan != null ? $pengajuan->status : '' }}' == 'complete'){
                $('.status-select').attr('readonly', true);
                $('.note-input').attr('readonly', true);
            }
        }
    }

    if (page == 'edit' && status == 'complete') {
        $('#btnSubmit').hide();
    }

    $('.status-select').change(function (e) { 
        let status = $(this).val();
        if(status == 'complete'){
            $('.note-input').attr('readonly', true);
            $('.note-input').val('');
            $('.permit-input').attr('readonly', false);
        }else{
            $('.note-input').attr('readonly', false);
            $('.permit-input').attr('readonly', true);
        }
        
    });
</script>
<script>
    $('#nextToStudy').click(function (e) { 
        e.preventDefault();
        $('#study').addClass('active show');
        $('#identity').removeClass('active show');
    });


    $('#nextToSupporting').click(function (e) { 
        e.preventDefault();
        $('#supporting').addClass('active show');
        $('#study').removeClass('active show');
    });
    
    $('#prevToIdentity').click(function (e) { 
        e.preventDefault();
        $('#identity').addClass('active show');
        $('#study').removeClass('active show');
    });


    $('#nextToDocument').click(function (e) { 
        e.preventDefault();
        $('#document').addClass('active show');
        $('#supporting').removeClass('active show');
    });
    $('#prevToStudy').click(function (e) { 
        e.preventDefault();
        $('#study').addClass('active show');
        $('#supporting').removeClass('active show');
    });

    $('#prevToSupporting').click(function (e) { 
        e.preventDefault();
        $('#supporting').addClass('active show');
        $('#document').removeClass('active show');
    });
</script>
@endsection