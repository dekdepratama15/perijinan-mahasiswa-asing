@extends('templates.main')
@section('title')
    Submission
@endsection
@section('page')
    Submission List
@endsection
@section('content')
<div class="card card-primary card-outline shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            <a class="btn btn-secondary mb-3" href="{{ url('submission/export') }}"><i
                class="fas fa-file-excel mr-2"></i>Export Data</a>
            <table class="table table-striped" id="myTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th width="40px">No</th>
                        <th>Name</th>
                        <th>NIM</th>
                        <th>Level</th>
                        <th>Country of Origin</th>
                        <th>Status</th>
                        <th data-priority="10001" width="15%" class="text-center">Action</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($pengajuans as $pengajuan)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $pengajuan->mahasiswa->nama_lengkap }}</td>
                            <td>{{ $pengajuan->mahasiswa->user->nim }}</td>
                            <td>{{ $pengajuan->jenjang }}</td>
                            <td>{{ $pengajuan->negara_asal }}</td>
                            <td class="text-capitalize">{{ $pengajuan->status }}</td>
                            <td width="100px">
                                <a href="submission/{{ $pengajuan->id }}" class="btn btn-secondary btn-width" data-toggle="tooltip" data-placement="top" title="Lihat Data"><i
                                    class="fas fa-eye"></i></a>
                                <a href="submission/{{ $pengajuan->id }}/edit" class="btn btn-warning btn-width" data-toggle="tooltip" data-placement="top" title="Ubah Data"><i
                                    class="fas fa-edit"></i></a>
                                <button type="submit" data-id="{{ $pengajuan->id }}" data-title="{{ $pengajuan->mahasiswa->nama_lengkap }}" data-token="{{ csrf_token() }}" class="btn btn-danger btn-width deletebtn" data-toggle="tooltip" data-placement="top" title="Hapus Data"><i
                                    class="fas fa-trash"></i></button>  
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function (){
        $("#myTable").on("click", ".deletebtn", function(e){
            e.preventDefault();
            var id = $(this).data("id");
            var token = $(this).data("token");
            var title = $(this).data("title");
            
            Swal.fire({
                title: 'Yakin?',
                html:
                        "Yakin hapus mahasiswa " +
                        "<br> <b>" +
                        title + 
                        "? </b>",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#0275d8',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yakin',
                cancelButtonText: 'Tidak'
            })
            .then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "delete",
                        url: '/submission/'+id,
                        data: {
                            "id": id,
                            "_method": 'DELETE',
                            "_token": token,
                        },
                        success: function (response) {
                            Swal.fire(
                                'Terhapus!',
                                response.status,
                                'success'
                            )
                            .then((result) => {
                                location.reload();
                            });

                        }
                    });
                }
            })
        });
    });
</script>
@endsection

