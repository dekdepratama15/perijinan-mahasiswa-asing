@extends('templates.main')
@section('title')
    Home
@endsection
@section('page')
    Home
@endsection
@section('content')
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <section class="col-12">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" style="max-height: 480px; object-fit: cover" src="{{ asset('storage/files/slider/slide1.jpg') }}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" style="max-height: 480px; object-fit: cover" src="{{ asset('storage/files/slider/slide2.jpg') }}" alt="Second slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </section>
        <section class="col-12">
            <div class="bg-white w-100 p-5">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 w-100 d-flex flex-column justify-content-center align-items-center pt-5 pb-5 text-center">
                        <h1 class="mb-5">
                            SIPIBE ITB STIKOM Bali
                        </h1>
                        <p>
                            SIPIBE Merupakan sistem pengajuan izin belajar untuk mahasiswa asing melakukan pengajuan izin belajar secara online di Institut Teknologi dan Bisnis (ITB) STIKOM Bali.
                        </p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 w-100 d-flex flex-column justify-content-center align-items-center pt-5 pb-5">
                        <iframe width="100%" height="300px" src="//youtube.com/embed/X8csfIwTZYY?showinfo=0&rel=0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </section>
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection
@section('scripts')

@endsection