@extends('templates.main')
@section('title')
    Information
@endsection
@section('page')
    Information Detail
@endsection
@section('content')
<div class="card shadow mb-4">
    <div class="card-body card-primary card-outline">
        <div class="tab-content" id="myTabContent">
            @include(
                        'informasi-form-detail.detail'
                    )
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $('.form-control').attr('readonly', true);
</script>
@endsection