<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-light-primary elevation-4">
    {{--
    <!-- Brand Logo -->
    <a href="" class="brand-link">
        <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light">Bintang Mas</span>
    </a> --}}

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('img/logo.png') }}" alt="Sistem Image">
            </div>
            <div class="info">
                <a href="{{ url("/dashboard") }}" class="d-block">SIPIBE</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
                @auth 
                    @if (auth()->user()->role == 'admin')
                        <li class="nav-item">
                            <a href="{{ url('/user') }}" class="nav-link {{ str_contains(url()->current(), 'user') ? 'active' : '' }}">
                                <i class="nav-icon fas fa-user-lock"></i>
                                <p>
                                    Foreign Student
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/admin') }}" class="nav-link {{ str_contains(url()->current(), 'admin') ? 'active' : '' }}">
                                <i class="nav-icon fas fa-user-tie"></i>
                                <p>
                                    Admin
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/submission') }}" class="nav-link {{ str_contains(url()->current(), 'submission') ? 'active' : '' }}">
                                <i class="nav-icon fas fa-book"></i>
                                <p>
                                    Submission
                                </p>
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a href="{{ url('/home') }}" class="nav-link {{ str_contains(url()->current(), 'home') ? 'active' : '' }}">
                                <i class="nav-icon fas fa-home"></i>
                                <p>
                                    Home
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/submission/create') }}" class="nav-link {{ str_contains(url()->current(), 'submission') ? 'active' : '' }}">
                                <i class="nav-icon fas fa-book"></i>
                                <p>
                                    Submission
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/information') }}" class="nav-link {{ str_contains(url()->current(), 'information') ? 'active' : '' }}">
                                <i class="nav-icon fas fa-info-circle"></i>
                                <p>
                                    Information
                                </p>
                            </a>
                        </li>
                    @endif
                    
                    <li class="nav-item">
                        <a href="{{ url('/logout') }}" class="nav-link">
                            <i class="nav-icon fas fa-sign-out-alt"></i>
                            <p>
                                Logout
                            </p>
                        </a>
                    </li>
                @endauth
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>