@extends('templates.main')
@section('title')
    Admin
@endsection
@section('page')
    Admin Details
@endsection
@section('content')
<div class="card card-primary card-outline shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            {{-- <a class="btn btn-primary mb-3" href="{{ url('transaction/create') }}"><i
                class="fas fa-plus mr-2"></i>Tambah Data</a> --}}
            <table class="table table-striped" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="50%">
                        <b>Username</b>
                    </td>
                    <td>
                        {{ $admin->user->username }}
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                        <b>Email</b>
                    </td>
                    <td>
                        {{ $admin->user->email }}
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                        <b>Name</b>
                    </td>
                    <td>
                        {{ $admin->nama_lengkap }}
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                        <b>Phone Number</b>
                    </td>
                    <td>
                        {{ $admin->no_telp }}
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                        <b>Gender</b>
                    </td>
                    <td>
                        {{ $admin->jenis_kelamin }}
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                        <b>Religion</b>
                    </td>
                    <td>
                        {{ $admin->agama }}
                    </td>
                </tr>
            </table>
            
            <a class="btn btn-outline-danger mt-3" href="{{ url('/admin') }}">Back</a>
        </div>
    </div>
</div>
@endsection
@section('scripts')

@endsection

