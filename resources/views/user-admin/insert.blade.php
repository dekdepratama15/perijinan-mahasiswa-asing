@extends('templates.main')
@section('title')
    Admin
@endsection
@section('page')
    Add New Admin
@endsection
@section('content')
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-body card-primary card-outline">
        <form method="POST" action="/admin" id="theForm" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label>Username <sup style="color: red">*</sup></label>
                        <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" id="username" value="{{ old('username') }}" placeholder="Username" autocomplete="off">
                            @error('username')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div> 
                            @enderror
                    </div>
                    <div class="form-group">
                        <label>Name<sup style="color: red">*</sup></label>
                        <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap" id="nama_lengkap" value="{{ old('nama_lengkap') }}" placeholder="Name" autocomplete="off">
                            @error('nama_lengkap')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div> 
                            @enderror
                    </div>
                    <div class="form-group">
                        <label>Gender <sup style="color: red">*</sup></label>
                        <select name="jenis_kelamin" class="form-control  @error('jenis_kelamin') is-invalid @enderror" id="jenis_kelamin">
                            <option value="" selected disabled>Select Option</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                            @error('jenis_kelamin')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div> 
                            @enderror
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" value="{{ old('password') }}" placeholder="Password" autocomplete="off">
                            @error('password')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div> 
                            @enderror
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" class="form-control @error('email') is-invalid @enderror" name="email" id="email" value="{{ old('email') }}" placeholder="Email" autocomplete="off">
                            @error('email')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div> 
                            @enderror
                    </div>
                    <div class="form-group">
                        <label>Phone Number <sup style="color: red">*</sup></label>
                        <input type="text" class="form-control @error('no_telp') is-invalid @enderror" name="no_telp" id="no_telp" value="{{ old('no_telp') }}" placeholder="Phone Number" autocomplete="off">
                            @error('no_telp')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div> 
                            @enderror
                    </div>
                    <div class="form-group">
                        <label>Religion <sup style="color: red">*</sup></label>
                        <select name="agama" class="form-control  @error('agama') is-invalid @enderror" id="agama">
                            <option value="" selected disabled>Select Option</option>
                            <option value="Hindu">Hindu</option>
                            <option value="Islam">Islam</option>
                            <option value="Buddha">Buddha</option>
                            <option value="Kristen Katolik">Kristen Katolik</option>
                            <option value="Kristen Protestan">Kristen Protestan</option>
                            <option value="Konghucu">Konghucu</option>
                        </select>
                            @error('agama')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div> 
                            @enderror
                    </div>
                </div>
            </div>
            
            <a class="btn btn-outline-danger mt-3" href="{{ url('/user') }}">Back</a>
            <button type="submit" id="btnSubmit" class="btn btn-primary ml-2 mt-3">Submit</button>
        </form>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $('.custom-file-input').change(function (e) {
        if (e.target.files.length) {
            $(this).next('.custom-file-label').html(e.target.files[0].name);
        }
    });
</script>
@endsection