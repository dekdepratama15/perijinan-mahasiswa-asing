@extends('templates.main')
@section('title')
    Admin
@endsection
@section('page')
    Admin List
@endsection
@section('content')
<div class="card card-primary card-outline shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            <a class="btn btn-primary mb-3" href="{{ url('admin/create') }}"><i
                class="fas fa-plus mr-2"></i>Add Data</a> 
            <table class="table table-striped" id="myTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th width="40px">No</th>
                        <th>Username</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th data-priority="10001" width="15%" class="text-center">Action</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($admins as $admin)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $admin->user->username }}</td>
                            <td>{{ $admin->nama_lengkap }}</td>
                            <td>{{ $admin->user->email }}</td>
                            <td width="100px">
                                <a href="admin/{{ $admin->id }}" class="btn btn-secondary btn-width" data-toggle="tooltip" data-placement="top" title="Lihat Data"><i
                                    class="fas fa-eye"></i></a>
                                @if(auth()->user()->id == $admin->user->id)
                                <a href="admin/{{ $admin->id }}/edit" class="btn btn-warning btn-width" data-toggle="tooltip" data-placement="top" title="Ubah Data"><i
                                    class="fas fa-edit"></i></a>
                                @endif
                                <button type="submit" data-id="{{ $admin->id }}" data-title="{{ $admin->user->username }}" data-token="{{ csrf_token() }}" class="btn btn-danger btn-width deletebtn" data-toggle="tooltip" data-placement="top" title="Hapus Data"><i
                                    class="fas fa-trash"></i></button>  
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function (){
        $("#myTable").on("click", ".deletebtn", function(e){
            e.preventDefault();
            var id = $(this).data("id");
            var token = $(this).data("token");
            var title = $(this).data("title");
            
            Swal.fire({
                title: 'Yakin?',
                html:
                        "Yakin hapus mahasiswa " +
                        "<br> <b>" +
                        title + 
                        "? </b>",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#0275d8',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yakin',
                cancelButtonText: 'Tidak'
            })
            .then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "delete",
                        url: '/user/'+id,
                        data: {
                            "id": id,
                            "_method": 'DELETE',
                            "_token": token,
                        },
                        success: function (response) {
                            Swal.fire(
                                'Terhapus!',
                                response.status,
                                'success'
                            )
                            .then((result) => {
                                location.reload();
                            });

                        }
                    });
                }
            })
        });
    });
</script>
@endsection

