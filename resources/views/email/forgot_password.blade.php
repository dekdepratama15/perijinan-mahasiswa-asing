<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="x-apple-disable-message-reformatting">
    <title></title>
    <style>
        table, td, div, h1, p {font-family: Arial, sans-serif;}
    </style>
</head>
<body style="margin:0;padding:0;">
    <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
    <tr>
    <td align="center" style="padding:0;">
        <table role="presentation" style="width:602px;border-collapse:collapse;border:1px solid #cccccc;border-spacing:0;text-align:left;">
            <tr>
                <td style="padding:36px 30px 0 30px;">
                    <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
                        <tr>
                            <td style="padding:0; color:#153643;">
                                <img src="{{ $message->embed(public_path('/img/logo.png')) }}" alt="SIPIBE Logo" class="brand-image" width="20%">
                                <h2>SIPIBE</h2>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding:36px 30px 42px 30px;">
                    <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
                        <tr>
                            <td style="padding:0; color:#153643;">
                                <h1 style="font-size:24px;margin:0 0 20px 0;font-family:Arial,sans-serif;">Hai, {{ $username }}</h1>
                                <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">You can reset password from button bellow.</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" style="padding:0 0 30px 0;">
                    <img width="300px" src="{{ $message->embed(public_path('/img/forgot_password.png')) }}" alt="">    
                </td>
            </tr>
            <tr>
                <td align="center" style="padding:0 0 30px 0;">
                    <a style="padding:10px 12px; border-radius: 8px; background-color:#080844; text-decoration: none; color:white; font-weight: bold;" href="{{ $url.'/reset-password/'.$token }}">Reset Password</a>
                </td>
            </tr>
            <tr>
                <td style="padding:36px 30px">
                    <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
                        <tr>
                            <td style="padding:0;color:#153643;">
                                <p style="margin:0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">This email was auto-generated, please do not reply.</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>