<div class="card">
    <div class="card-header">
        <label for="">Identity</label>
    </div>
    <div class="card-body p-3">
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>Name <sup style="color: red">*</sup></label>
                    <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap" id="nama_lengkap" value="{{ $pengajuan->mahasiswa->nama_lengkap }}" readonly placeholder="Name" autocomplete="off">
                        @error('nama_lengkap')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Place of Birth <sup style="color: red">*</sup></label>
                    <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" name="tempat_lahir" id="tempat_lahir" value="{{ $pengajuan->tempat_lahir }}" placeholder="Place of Birth" autocomplete="off">
                        @error('tempat_lahir')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Date of Birth <sup style="color: red">*</sup></label>
                    <input type="date" class="form-control @error('tanggal_lahir') is-invalid @enderror" name="tanggal_lahir" id="tanggal_lahir" value="{{ $pengajuan->tanggal_lahir }}" placeholder="Date of Birth" autocomplete="off">
                        @error('tanggal_lahir')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Gender <sup style="color: red">*</sup></label>
                    <select name="jenis_kelamin" class="form-control  @error('jenis_kelamin') is-invalid @enderror" readonly id="jenis_kelamin">
                        <option value="" selected disabled>Select Option</option>
                        <option value="Male" @if ($pengajuan->mahasiswa->jenis_kelamin == 'Male') selected @endif>Male</option>
                        <option value="Female" @if ($pengajuan->mahasiswa->jenis_kelamin == 'Female') selected @endif>Female</option>
                    </select>
                        @error('jenis_kelamin')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Nationality <sup style="color: red">*</sup></label>
                    <input type="text" class="form-control @error('kebangsaan') is-invalid @enderror" name="kebangsaan" id="kebangsaan" value="{{ $pengajuan->kebangsaan }}" placeholder="Nationality" autocomplete="off">
                        @error('kebangsaan')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Religion <sup style="color: red">*</sup></label>
                    <select name="agama" class="form-control  @error('agama') is-invalid @enderror" id="agama">
                        <option value="" selected disabled>Select Option</option>
                        <option value="Hindu" @if ($pengajuan->mahasiswa->agama == 'Hindu') selected @endif>Hindu</option>
                        <option value="Islam" @if ($pengajuan->mahasiswa->agama == 'Islam') selected @endif>Islam</option>
                        <option value="Buddha" @if ($pengajuan->mahasiswa->agama == 'Buddha') selected @endif>Buddha</option>
                        <option value="Kristen Katolik" @if ($pengajuan->mahasiswa->agama == 'Kristen Katolik') selected @endif>Kristen Katolik</option>
                        <option value="Kristen Protestan" @if ($pengajuan->mahasiswa->agama == 'Kristen Protestan') selected @endif>Kristen Protestan</option>
                        <option value="Konghucu" @if ($pengajuan->mahasiswa->agama == 'Konghucu') selected @endif>Konghucu</option>
                    </select>
                        @error('agama')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <label for="">Original Residence</label>
    </div>
    <div class="card-body p-3">
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>City <sup style="color: red">*</sup></label>
                    <input type="text" class="form-control @error('kota_asal') is-invalid @enderror" name="kota_asal" id="kota_asal" value="{{ $pengajuan->kota_asal }}" placeholder="City" autocomplete="off">
                        @error('kota_asal')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Province/State <sup style="color: red">*</sup></label>
                    <input type="text" class="form-control @error('provinsi_asal') is-invalid @enderror" name="provinsi_asal" id="provinsi_asal" value="{{ $pengajuan->provinsi_asal }}" placeholder="Province/State" autocomplete="off">
                        @error('provinsi_asal')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Country <sup style="color: red">*</sup></label>
                    <input type="text" class="form-control @error('negara_asal') is-invalid @enderror" name="negara_asal" id="negara_asal" value="{{ $pengajuan->negara_asal }}" placeholder="Country" autocomplete="off">
                        @error('negara_asal')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Postal Code <sup style="color: red">*</sup></label>
                    <input type="text" class="form-control @error('kode_pos_asal') is-invalid @enderror" name="kode_pos_asal" id="kode_pos_asal" value="{{ $pengajuan->kode_pos_asal }}" placeholder="Postal Code" autocomplete="off">
                        @error('kode_pos_asal')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label>Address <sup style="color: red">*</sup></label>
                    <textarea class="form-control @error('alamat_asal') is-invalid @enderror" name="alamat_asal" id="alamat_asal" cols="30" rows="3">{{ $pengajuan->alamat_asal }}</textarea>
                        @error('alamat_asal')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <label for="">Residence in Indonesia</label>
    </div>
    <div class="card-body p-3">
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>City <sup style="color: red">*</sup></label>
                    <input type="text" class="form-control @error('kota_tinggal') is-invalid @enderror" name="kota_tinggal" id="kota_tinggal" value="{{ $pengajuan->kota_tinggal }}" placeholder="City" autocomplete="off">
                        @error('kota_tinggal')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Province/State <sup style="color: red">*</sup></label>
                    <input type="text" class="form-control @error('provinsi_tinggal') is-invalid @enderror" name="provinsi_tinggal" id="provinsi_tinggal" value="{{ $pengajuan->provinsi_tinggal }}" placeholder="Province/State" autocomplete="off">
                        @error('provinsi_tinggal')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Postal Code <sup style="color: red">*</sup></label>
                    <input type="text" class="form-control @error('kode_pos_tinggal') is-invalid @enderror" name="kode_pos_tinggal" id="kode_pos_tinggal" value="{{ $pengajuan->kode_pos_tinggal }}" placeholder="Postal Code" autocomplete="off">
                        @error('kode_pos_tinggal')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Email <sup style="color: red">*</sup></label>
                    <input type="email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" class="form-control @error('email') is-invalid @enderror" name="email" id="email" readonly value="{{ $pengajuan->mahasiswa->user->email }}" placeholder="Email" autocomplete="off">
                        @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Phone Number <sup style="color: red">*</sup></label>
                    <input type="text" class="form-control @error('no_telp') is-invalid @enderror" name="no_telp" id="no_telp" value="{{ $pengajuan->mahasiswa->no_telp }}" readonly placeholder="Phone Number" autocomplete="off">
                        @error('no_telp')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label>Address <sup style="color: red">*</sup></label>
                    <textarea class="form-control @error('alamat_tinggal') is-invalid @enderror" name="alamat_tinggal" id="alamat_tinggal" cols="30" rows="3">{{ $pengajuan->alamat_tinggal }}</textarea>
                        @error('alamat_tinggal')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
        </div>
    </div>
</div>

@if (auth()->user()->role == 'admin')
<div class="card">
    <div class="card-header">
        <label for="">Submmision Detail</label>
    </div>
    <div class="card-body p-3">
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>Status <sup style="color: red">*</sup></label>
                    <select name="status" class="form-control status-select  @error('status') is-invalid @enderror" id="status">
                        <option value="" selected disabled>Select Option</option>
                        <option value="complete" @if (old('status', $pengajuan->status) == 'complete') selected @endif>Complete</option>
                        <option value="rejected" @if (old('status', $pengajuan->status) == 'rejected') selected @endif>Rejected</option>
                    </select>
                    @error('status')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div> 
                    @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Study Permit Letter</label>
                    <input type="file" class="form-control permit-input @error('surat_ijin_belajar') is-invalid @enderror" name="surat_ijin_belajar" value="{{ old('surat_ijin_belajar') }}" placeholder="Study Permit Letter"autocomplete="off">
                        @error('surat_ijin_belajar')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label>Notes <sup style="color: red">*</sup></label>
                    <textarea class="form-control note-input @error('note') is-invalid @enderror" name="note" id="note" cols="30" rows="3">{{ $pengajuan->note }}</textarea>
                        @error('note')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
        </div>
    </div>
</div>
@else
<input type="hidden" name='status' value="{{ $pengajuan->status}}">
<input type="hidden" name='note' value="{{ $pengajuan->note}}">
@endif

@if (auth()->user()->role == 'user')
    <button type="button" id="nextToStudy" class="btn btn-secondary ml-2 mt-5 float-right">Next</button>
@endif
