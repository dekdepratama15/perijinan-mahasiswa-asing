
<div class="row p-3">
    <div class="col-6">
        <div class="form-group">
            <label>Financial Guarantee Letter</label>
            <input type="file" class="form-control @error('surat_jaminan_keuangan') is-invalid @enderror" name="surat_jaminan_keuangan" id="surat_jaminan_keuangan" value="{{ old('surat_jaminan_keuangan') }}" placeholder="Financial Guarantee Letter" autocomplete="off">
                @error('surat_jaminan_keuangan')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div> 
                @enderror
            <div>
                <a href="{{ $pengajuan->surat_jaminan_keuangan }}" target="_blank">Financial Guarantee Letter</a>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Statement Letter</label>
            <input type="file" class="form-control @error('surat_pernyataan') is-invalid @enderror" name="surat_pernyataan" id="surat_pernyataan" value="{{ old('surat_pernyataan') }}" placeholder="Statement Letter" autocomplete="off">
                @error('surat_pernyataan')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div> 
                @enderror
            <div>
                <a href="{{ $pengajuan->surat_pernyataan }}" target="_blank">Statement Letter</a>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Health Document</label>
            <input type="file" class="form-control @error('surat_kesehatan') is-invalid @enderror" name="surat_kesehatan" id="surat_kesehatan" value="{{ old('surat_kesehatan') }}" placeholder="Health Document" autocomplete="off">
                @error('surat_kesehatan')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div> 
                @enderror
            <div>
                <a href="{{ $pengajuan->surat_kesehatan }}" target="_blank">Health Document</a>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Letter of Acceptance</label>
            <input type="file" class="form-control @error('letter_of_accept') is-invalid @enderror" name="letter_of_accept" id="letter_of_accept" value="{{ old('letter_of_accept') }}" placeholder="Letter of Acceptance" autocomplete="off">
                @error('letter_of_accept')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div> 
                @enderror
            <div>
                <a href="{{ $pengajuan->letter_of_accept }}" target="_blank">Letter of Acceptance</a>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Last Study Certificate</label>
            <input type="file" class="form-control @error('ijasah_terakhir') is-invalid @enderror" name="ijasah_terakhir" id="ijasah_terakhir" value="{{ old('ijasah_terakhir') }}" placeholder="Last Study Certificate" autocomplete="off">
                @error('ijasah_terakhir')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div> 
                @enderror
            <div>
                <a href="{{ $pengajuan->ijasah_terakhir }}" target="_blank">Last Study Certificate</a>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Photo</label>
            <input type="file" class="form-control @error('foto') is-invalid @enderror" name="foto" id="foto" value="{{ old('foto') }}" placeholder="Last Study Certificate" autocomplete="off">
                @error('foto')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div> 
                @enderror
            <p class="text-secondary">*Upload photo with ratio 3x4.</p>
            <div>
                <a href="{{ $pengajuan->foto }}" target="_blank">Photo</a>
            </div>
        </div>
    </div>
    {{-- <div class="col-6">
        <div class="form-group">
            <label>Study Permit Letter</label>
            <input type="file" class="form-control @error('surat_ijin_belajar') is-invalid @enderror" name="surat_ijin_belajar" id="surat_ijin_belajar" value="{{ old('surat_ijin_belajar') }}" placeholder="Study Permit Letter" autocomplete="off">
                @error('surat_ijin_belajar')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div> 
                @enderror
            <div>
                <a href="{{ $pengajuan->surat_ijin_belajar }}" target="_blank">Study Permit Letter</a>
            </div>
        </div>
    </div> --}}
</div>
@if (auth()->user()->role == 'user')
    <button type="button" id="prevToSupporting" class="btn btn-secondary mt-5 float-left">Previous</button>
    <button type="submit" id="btnSubmit" class="btn btn-primary ml-2 mt-5 float-right">Submit</button>
@endif
