
<div class="row p-3">
    <div class="col-6">
        <div class="form-group">
            <label>Financial Guarantee Letter</label>
            <div>
                <a href="{{ $pengajuan->surat_jaminan_keuangan }}" target="_blank">Financial Guarantee Letter</a>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Statement Letter</label>
            <div>
                <a href="{{ $pengajuan->surat_pernyataan }}" target="_blank">Statement Letter</a>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Health Document</label>
            <div>
                <a href="{{ $pengajuan->surat_kesehatan }}" target="_blank">Health Document</a>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Letter of Acceptance</label>
            <div>
                <a href="{{ $pengajuan->letter_of_accept }}" target="_blank">Letter of Acceptance</a>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Last Study Certificate</label>
            <div>
                <a href="{{ $pengajuan->ijasah_terakhir }}" target="_blank">Last Study Certificate</a>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Photo</label>
            <div>
                <a href="{{ $pengajuan->foto }}" target="_blank">Photo</a>
            </div>
        </div>
    </div>
    {{-- <div class="col-6">
        <div class="form-group">
            <label>Study Permit Letter</label>
            <div>
                <a href="{{ $pengajuan->surat_ijin_belajar }}" target="_blank">Study Permit Letter</a>
            </div>
        </div>
    </div> --}}
</div>
