<div class="card">
    <div class="card-header">
        <label for="">Identity</label>
    </div>
    <div class="card-body p-3">
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap" id="nama_lengkap" value="{{ $pengajuan->mahasiswa->nama_lengkap }}" placeholder="Name" autocomplete="off">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Place of Birth</label>
                    <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" name="tempat_lahir" id="tempat_lahir" value="{{ $pengajuan->tempat_lahir }}" placeholder="Place of Birth" autocomplete="off">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Date of Birth</label>
                    <input type="date" class="form-control @error('tanggal_lahir') is-invalid @enderror" name="tanggal_lahir" id="tanggal_lahir" value="{{ $pengajuan->tanggal_lahir }}" placeholder="Date of Birth" autocomplete="off">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Gender</label>
                    <select name="jenis_kelamin" class="form-control  @error('jenis_kelamin') is-invalid @enderror" id="jenis_kelamin">
                        <option value="" selected disabled>Select Option</option>
                        <option value="Male" @if ($pengajuan->mahasiswa->jenis_kelamin == 'Male') selected @endif>Male</option>
                        <option value="Female" @if ($pengajuan->mahasiswa->jenis_kelamin == 'Female') selected @endif>Female</option>
                    </select>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Nationality</label>
                    <input type="text" class="form-control @error('kebangsaan') is-invalid @enderror" name="kebangsaan" id="kebangsaan" value="{{ $pengajuan->kebangsaan }}" placeholder="Nationality" autocomplete="off">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Religion <sup style="color: red">*</sup></label>
                    <select name="agama" class="form-control  @error('agama') is-invalid @enderror" id="agama">
                        <option value="" selected disabled>Select Option</option>
                        <option value="Hindu" @if ($pengajuan->mahasiswa->agama == 'Hindu') selected @endif>Hindu</option>
                        <option value="Islam" @if ($pengajuan->mahasiswa->agama == 'Islam') selected @endif>Islam</option>
                        <option value="Buddha" @if ($pengajuan->mahasiswa->agama == 'Buddha') selected @endif>Buddha</option>
                        <option value="Kristen Katolik" @if ($pengajuan->mahasiswa->agama == 'Kristen Katolik') selected @endif>Kristen Katolik</option>
                        <option value="Kristen Protestan" @if ($pengajuan->mahasiswa->agama == 'Kristen Protestan') selected @endif>Kristen Protestan</option>
                        <option value="Konghucu" @if ($pengajuan->mahasiswa->agama == 'Konghucu') selected @endif>Konghucu</option>
                    </select>
                        @error('agama')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div> 
                        @enderror
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <label for="">Original Residence</label>
    </div>
    <div class="card-body p-3">
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>City</label>
                    <input type="text" class="form-control @error('kota_asal') is-invalid @enderror" name="kota_asal" id="kota_asal" value="{{ $pengajuan->kota_asal }}" placeholder="City" autocomplete="off">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Province/State</label>
                    <input type="text" class="form-control @error('provinsi_asal') is-invalid @enderror" name="provinsi_asal" id="provinsi_asal" value="{{ $pengajuan->provinsi_asal }}" placeholder="Province/State" autocomplete="off">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Country</label>
                    <input type="text" class="form-control @error('negara_asal') is-invalid @enderror" name="negara_asal" id="negara_asal" value="{{ $pengajuan->negara_asal }}" placeholder="Country" autocomplete="off">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Postal Code</label>
                    <input type="text" class="form-control @error('kode_pos_asal') is-invalid @enderror" name="kode_pos_asal" id="kode_pos_asal" value="{{ $pengajuan->kode_pos_asal }}" placeholder="Postal Code" autocomplete="off">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label>Address</label>
                    <textarea class="form-control @error('alamat_asal') is-invalid @enderror" name="alamat_asal" id="alamat_asal" cols="30" rows="3">{{ $pengajuan->alamat_asal }}</textarea>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <label for="">Residence in Indonesia</label>
    </div>
    <div class="card-body p-3">
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>City</label>
                    <input type="text" class="form-control @error('kota_tinggal') is-invalid @enderror" name="kota_tinggal" id="kota_tinggal" value="{{ $pengajuan->kota_tinggal }}" placeholder="City" autocomplete="off">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Province/State</label>
                    <input type="text" class="form-control @error('provinsi_tinggal') is-invalid @enderror" name="provinsi_tinggal" id="provinsi_tinggal" value="{{ $pengajuan->provinsi_tinggal }}" placeholder="Province/State" autocomplete="off">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Postal Code</label>
                    <input type="text" class="form-control @error('kode_pos_tinggal') is-invalid @enderror" name="kode_pos_tinggal" id="kode_pos_tinggal" value="{{ $pengajuan->kode_pos_tinggal }}" placeholder="Postal Code" autocomplete="off">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Email</label>
                    <input type="email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" class="form-control @error('email') is-invalid @enderror" name="email" id="email" value="{{ $pengajuan->mahasiswa->user->email }}" placeholder="Email" autocomplete="off">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Phone Number</label>
                    <input type="text" class="form-control @error('no_telp') is-invalid @enderror" name="no_telp" id="no_telp" value="{{ $pengajuan->mahasiswa->no_telp }}" placeholder="Phone Number" autocomplete="off">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label>Address</label>
                    <textarea class="form-control @error('alamat_tinggal') is-invalid @enderror" name="alamat_tinggal" id="alamat_tinggal" cols="30" rows="3">{{ $pengajuan->alamat_tinggal }}</textarea>
                </div>
            </div>
        </div>
    </div>
</div>