
<div class="row p-3">
    <div class="col-6">
        <div class="form-group">
            <label>Passport Number</label>
            <input type="text" class="form-control @error('no_paspor') is-invalid @enderror" name="no_paspor" id="no_paspor" value="{{ $pengajuan->no_paspor }}" placeholder="Passport Number" autocomplete="off">
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Date of Issue</label>
            <input type="date" class="form-control @error('tgl_pengajuan_paspor') is-invalid @enderror" name="tgl_pengajuan_paspor" id="tgl_pengajuan_paspor" value="{{ $pengajuan->tgl_pengajuan_paspor }}" placeholder="Date of Issue" autocomplete="off">
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Date of Expired</label>
            <input type="date" class="form-control @error('tgl_berakhir') is-invalid @enderror" name="tgl_berakhir" id="tgl_berakhir" value="{{ $pengajuan->tgl_berakhir }}" placeholder="Date of Expired" autocomplete="off">
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Type of Funding</label>
            <select name="jenis_pengadaan" class="form-control  @error('jenis_pengadaan') is-invalid @enderror" id="jenis_pengadaan">
                <option value="" selected disabled>Select Option</option>
                <option value="Independent Fees"  @if ($pengajuan->jenis_pengadaan == 'Independent Fees') selected @endif>Independent Fees</option>
                <option value="Scholarships"  @if ($pengajuan->jenis_pengadaan == 'Scholarships') selected @endif>Scholarships</option>
                <option value="Others"  @if ($pengajuan->jenis_pengadaan == 'Others') selected @endif>Others</option>
            </select>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Scholarship Provider</label>
            <input type="text" class="form-control @error('penyedia_beasiswa') is-invalid @enderror" name="penyedia_beasiswa" id="penyedia_beasiswa" value="{{ $pengajuan->penyedia_beasiswa }}" placeholder="Scholarship Provider" autocomplete="off">
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Guarantor Position</label>
            <input type="text" class="form-control @error('jabatan_penjamin') is-invalid @enderror" name="jabatan_penjamin" id="jabatan_penjamin" value="{{ $pengajuan->jabatan_penjamin }}" placeholder="Guarantor Position" autocomplete="off">
        </div>
    </div>
</div>
