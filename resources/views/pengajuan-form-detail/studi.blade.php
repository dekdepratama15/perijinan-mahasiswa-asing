<div class="card">
    <div class="card-header">
        <label for="">Study Information</label>
    </div>
    <div class="card-body p-3">
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>Education Level</label>
                    <select name="jenjang" class="form-control  @error('jenjang') is-invalid @enderror" id="jenjang">
                        <option value="" selected disabled>Select Option</option>
                        <option value="D3" @if ($pengajuan->jenjang == 'D3') selected @endif>D3</option>
                        <option value="S1" @if ($pengajuan->jenjang == 'S1') selected @endif>S1</option>
                        <option value="S2" @if ($pengajuan->jenjang == 'S2') selected @endif>S2</option>
                    </select>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Level of Study</label>
                    <input type="number" class="form-control @error('angkatan') is-invalid @enderror" name="angkatan" id="angkatan" value="{{ $pengajuan->angkatan }}" placeholder="Name" readonly autocomplete="off">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Study Program</label>
                    <select name="prodi" class="form-control  @error('prodi') is-invalid @enderror" id="prodi">
                        <option value="" selected disabled>Select Option</option>
                        <option value="Sistem Informasi" @if ($pengajuan->prodi == 'Sistem Informasi') selected @endif>Sistem Informasi</option>
                        <option value="Sistem Komputer" @if ($pengajuan->prodi == 'Sistem Komputer') selected @endif>Sistem Komputer</option>
                        <option value="Teknologi Informasi" @if ($pengajuan->prodi == 'Teknologi Informasi') selected @endif>Teknologi Informasi</option>
                        <option value="Bisnis Digital" @if ($pengajuan->prodi == 'Bisnis Digital') selected @endif>Bisnis Digital</option>
                        <option value="Manajemen Informatika" @if ($pengajuan->prodi == 'Manajemen Informatika') selected @endif>Manajemen Informatika</option>
                        <option value="Internasional Dual Degree" @if ($pengajuan->prodi == 'Internasional Dual Degree') selected @endif>Internasional Dual Degree</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <label for="">Study Permit Period</label>
    </div>
    <div class="card-body p-3">
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>Start Learning</label>
                    <input type="date" class="form-control @error('mulai_ijin_belajar') is-invalid @enderror" name="mulai_ijin_belajar" id="mulai_ijin_belajar" value="{{ $pengajuan->mulai_ijin_belajar }}" placeholder="Start Learning" autocomplete="off">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Long Study Permit</label>
                    <input type="date" class="form-control @error('lama_ijin_belajar') is-invalid @enderror" name="lama_ijin_belajar" id="lama_ijin_belajar" value="{{ $pengajuan->lama_ijin_belajar }}" placeholder="Long Study Permit" autocomplete="off">
                </div>
            </div>
        </div>
    </div>
</div>