<div class="card">
    <div class="card-header">
        <label for="">Activity</label>
    </div>
    <div class="card-body p-3">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <div class="text-center">
                        @if (isset($pengajuan->status)) 
                            @if ($pengajuan->status == 'checking') 
                                Your application has been successful, please wait for the checking process from student affairs.
                            @elseif ($pengajuan->status == 'rejected') 
                                Your application has been rejected, please read the student affairs provisions and re-submit.
                            @elseif ($pengajuan->status == 'complete') 
                                Your submission is completed {{ !empty($pengajuan->tanggal_verifikasi) ? date('d M Y, H:i', strtotime($pengajuan->tanggal_verifikasi)).'.' : '.' }}
                            @endif 
                        @else
                            Please make a submission by filling in the form below.
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <label for="">Information Detail</label>
    </div>
    <div class="card-body p-3">
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>Status</label>
                    <div>
                        @if (isset($pengajuan->status))
                            <div class="badge badge-info text-capitalize">{{ $pengajuan->status }}</div>
                        @else
                            <div class="badge badge-info text-capitalize">Not Submmited</div>
                        @endif
                    </div>
                </div>
            </div>
            @if (isset($pengajuan->status))
                @if ($pengajuan->status == 'complete')
                    <div class="col-12">
                        <div class="form-group">
                            <label>Study Permit Letter</label>
                            <div>
                                <a href="{{ $pengajuan->surat_ijin_belajar }}" target="_blank">Study Permit Letter</a>
                            </div>
                        </div>
                    </div>
                @endif
            @endif
            @if($admin != null)
                <div class="col-12">
                    <div class="form-group">
                        <label>Admin Last Change</label>
                        <div>
                            <p>{{ $admin->nama_lengkap }}</p>
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-12">
                <div class="form-group">
                    <label>Notes</label>
                    <textarea class="form-control @error('note') is-invalid @enderror" name="note" id="note" cols="30" rows="3">@if (isset($pengajuan)){{ $pengajuan->note ? $pengajuan->note : '-' }}@endif
                    </textarea>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <label for="">Application Status Log</label>
    </div>
    <div class="card-body p-3">
        <div class="row">
            <div class="col-12">
            <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Date</th>
                            <th scope="col">User</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($pengajuan->pengajuan_logs) > 0)
                            @foreach($pengajuan->pengajuan_logs as $pengajuan_log)
                                <tr>
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ date('d M Y, H:i', strtotime($pengajuan_log->tanggal)) }}</td>
                                    <td>{{ $pengajuan_log->user_change }} ({{ $pengajuan_log->user_change_role }})</td>
                                    <td>
                                        @if($pengajuan_log->status_lama == $pengajuan_log->status_baru)
                                            {{ ucfirst($pengajuan_log->status_lama) }}
                                        @else
                                            {{ ucfirst($pengajuan_log->status_lama) }} => {{ ucfirst($pengajuan_log->status_baru) }}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4" class="text-center">No have data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>